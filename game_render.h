enum Materials
{
    DIFFUSE,
    METAL,
};

struct Obj {
    v4 *vertices;
    v4 *vertexts;
    v4 *normals;

    v4 *vert_indices;
    v4 *vertext_indices;
    v4 *normal_indices;
 
    s32 vert_num;
    s32 vertext_num;
    s32 normal_num;
    s32 face_num;
};

enum Object_Type {
    FLOOR,
    CHARACTER,
    CUBE,
    TARGET_CIRCLE,
    OBJECT_COUNT,
};

enum Render_Options {
    SHADOW_OFF,
    SHADOW_ON,
};

enum Projection_Type {
    PERSPECTIVE,
    ORTHOGRAPHIC,
};

struct Rect {
    u8 *data;

    union {
        struct {
            f32 bl, tl, tr, br;
        };

        struct {
            f32 a, b, c ,d;
        };
        
        f32 e[4];
    };

    s32 width, height, area;
    
    s32 x0, x1, y0, y1;
    s32 size;
};

struct Shape_Rect {
    f32 x0, y0, x1, y1;
};

// TODO: Clean up some of the members.
struct Camera {
    // @vars
    struct {
        f32 rotate_x = 0;
        f32 rotate_y = 0;
        f32 rotate_z = 0;

        f32 translate_x = 0;
        f32 translate_y = 0;
        f32 translate_z = 0;
    };

    s32 zdir = 1; // Direction multiplier
    f32 fov;

    v4 light_dir;

    m4x4 viewport;
    m4x4 projection;

    m4x4 rotate;
    m4x4 translate;

    m4x4 lookat;
    m4x4 lookat_rotate;
    m4x4 lookat_translate;
    m4x4 lookat_tilt;
};

struct Texture {
    u8 *data;
    s32 width; 
    s32 height; 
    s32 n_channel;
};

struct Mesh {
    v4 *vertices;
    v4 *normals;
    v4 *vertexts;

    v4 *vert_indices;
    v4 *vertext_indices;
    v4 *normal_indices;

    s32 color;
    
    s32 face_num;

    m4x4 transform;

    bool enable_draw;
    Mesh *circle;
    Object_Type obj_type;
    Materials mat_type;
    Texture texture;

    v4 world_position;

    // Screen info
    Shape_Rect bounding_box = {};
};

struct Renderer {
    Rect    *screen;
    f32     *zbuffer;
    f32     *shadow_zbuffer;
    Mesh    *meshes;
    Obj     *objects;
    Texture *textures;

    bool do_depth;

    Camera camera;

    s32 mesh_count;
    s32 texture_count;

    m4x4 shadow_transform;
    Render_Options options;
};
