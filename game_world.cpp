// Return the tile at calculated index.
static Tile *
GetTile(Tile_Map *tile_map, v3 tile_pos)
{
    Tile *return_tile = NULL;

    // Convert position of a tile from tile map to a translation matrix suitable for opengl coordinates.
    s32 tile_index = tile_map->col_count * (s32)tile_pos.y + (s32)tile_pos.x;
    if (tile_index >= tile_map->tile_count || tile_index < 0) return_tile = NULL;
    else 
    {
        return_tile = &tile_map->tiles[tile_index];
        f32 x_visual_position = tile_map->tile_width  * (tile_pos.x - tile_map->col_count/2);
        f32 y_visual_position = tile_map->tile_height * (tile_pos.y - tile_map->row_count/2);

        return_tile->to_visual_position.m[3]  = x_visual_position;
        return_tile->to_visual_position.m[7]  = y_visual_position;
        return_tile->to_visual_position.m[11] = tile_pos.z;
    }

    return return_tile;
}


