#include "engine_matrix.cpp"
#include <gl/gl.h>
#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#endif

#define GL_ARRAY_BUFFER                   0x8892
#define GL_ARRAY_BUFFER_BINDING           0x8894
#define GL_COLOR_ATTACHMENT0              0x8CE0
#define GL_COMPILE_STATUS                 0x8B81
#define GL_CURRENT_PROGRAM                0x8B8D
#define GL_DYNAMIC_DRAW                   0x88E8
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_ELEMENT_ARRAY_BUFFER_BINDING   0x8895
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_FRAMEBUFFER                    0x8D40
#define GL_FRAMEBUFFER_COMPLETE           0x8CD5
#define GL_FUNC_ADD                       0x8006
#define GL_INVALID_FRAMEBUFFER_OPERATION  0x0506
#define GL_MAJOR_VERSION                  0x821B
#define GL_MINOR_VERSION                  0x821C
#define GL_STATIC_DRAW                    0x88E4
#define GL_STREAM_DRAW                    0x88E0
#define GL_TEXTURE0                       0x84C0
#define GL_VERTEX_SHADER                  0x8B31
#define GL_STREAM_READ                    0x88E1
#define GL_STREAM_COPY                    0x88E2
#define GL_STATIC_DRAW                    0x88E4
#define GL_STATIC_READ                    0x88E5
#define GL_STATIC_COPY                    0x88E6
#define GL_CLAMP_TO_EDGE                  0x812F
#define GL_CLAMP_TO_BORDER                0x812D
// Define list of opengl function declaration in opengl32.dll
#define GL_DCL_LIST \
/* ret, name, args*/ \
    GL_DCL(void,      LinkProgram,             GLuint program) \
    GL_DCL(void,      GetProgramiv,            GLuint program, GLenum pname, GLint *params) \
    GL_DCL(GLuint,    CreateShader,            GLenum type) \
    GL_DCL(void,      ShaderSource,            GLuint shader, GLsizei count, const GLchar* const *string, const GLint *length) \
    GL_DCL(void,      CompileShader,           GLuint shader) \
    GL_DCL(void,      GetShaderiv,             GLuint shader, GLenum pname, GLint *params) \
    GL_DCL(void,      GetShaderInfoLog,        GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) \
    GL_DCL(void,      DeleteShader,            GLuint shader) \
    GL_DCL(GLuint,    CreateProgram,           void) \
    GL_DCL(void,      AttachShader,            GLuint program, GLuint shader) \
    GL_DCL(void,      DetachShader,            GLuint program, GLuint shader) \
    GL_DCL(void,      UseProgram,              GLuint program) \
    GL_DCL(void,      DeleteProgram,           GLuint program) \
    GL_DCL(void,      GenVertexArrays,         GLsizei n, GLuint *arrays) \
    GL_DCL(void,      BindVertexArray,         GLuint array) \
    GL_DCL(void,      BufferData,              GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage) \
    GL_DCL(void,      GenBuffers,              GLsizei n, GLuint *buffers) \
    GL_DCL(void,      BindBuffer,              GLenum target, GLuint buffer) \
    GL_DCL(void,      DeleteBuffers,           GLsizei n, const GLuint *buffers) \
    GL_DCL(void,      ActiveTexture,           GLenum texture) \
    GL_DCL(void,      BindAttribLocation,      GLuint program, GLuint index, const GLchar *name) \
    GL_DCL(GLint,     GetUniformLocation,      GLuint program, const GLchar *name) \
    GL_DCL(void,      Uniform4f,               GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) \
    GL_DCL(void,      Uniform4fv,              GLint location, GLsizei count, const GLfloat *value) \
    GL_DCL(void,      DeleteVertexArrays,      GLsizei n, const GLuint *arrays) \
    GL_DCL(void,      EnableVertexAttribArray, GLuint index) \
    GL_DCL(void,      VertexAttribPointer,     GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer) \
    GL_DCL(void,      Uniform1i,               GLint location, GLint v0) \
    GL_DCL(void,      Uniform1f,               GLint location, GLfloat v0) \
    GL_DCL(void,      GenerateMipmap,          GLenum target) \
    GL_DCL(void,      UniformMatrix4fv,        GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) \
// Declare gl functions
#define GL_DCL(ret, name, ...) typedef ret gl##name##_(__VA_ARGS__);
GL_DCL_LIST
#undef GL_DCL
/* end */

// Functions that comes with gl/gl.h
typedef void glBindTexture_(GLenum target, GLuint texture); 
#define OPENGLMEMBERFUNCTION(name) gl##name##_* gl##name;

struct Opengl
{
    f32 vertices[32]; // TODO: Should be resizable array
    GLuint vbo, vao, ebo;
    const char* vshadersrc;
    const char* fshadersrc;
    GLuint texture;
    i32 width, height, nrchannels;
    unsigned char *data;
    u32 vertexshader, fragmentshader;
    int success;
    char infolog[512];
    u32 shaderProgram;
    HDC deviceContext;
    HGLRC openGLRenderContext;
    OPENGLMEMBERFUNCTION(LinkProgram);            
    OPENGLMEMBERFUNCTION(GetProgramiv);           
    OPENGLMEMBERFUNCTION(CreateShader);           
    OPENGLMEMBERFUNCTION(ShaderSource);           
    OPENGLMEMBERFUNCTION(CompileShader);          
    OPENGLMEMBERFUNCTION(GetShaderiv);            
    OPENGLMEMBERFUNCTION(GetShaderInfoLog);       
    OPENGLMEMBERFUNCTION(DeleteShader);           
    OPENGLMEMBERFUNCTION(CreateProgram);          
    OPENGLMEMBERFUNCTION(AttachShader);           
    OPENGLMEMBERFUNCTION(DetachShader);           
    OPENGLMEMBERFUNCTION(UseProgram);             
    OPENGLMEMBERFUNCTION(DeleteProgram);          
    OPENGLMEMBERFUNCTION(GenVertexArrays);        
    OPENGLMEMBERFUNCTION(BindVertexArray);        
    OPENGLMEMBERFUNCTION(BufferData);             
    OPENGLMEMBERFUNCTION(GenBuffers);             
    OPENGLMEMBERFUNCTION(BindBuffer);             
    OPENGLMEMBERFUNCTION(DeleteBuffers);          
    OPENGLMEMBERFUNCTION(ActiveTexture);          
    OPENGLMEMBERFUNCTION(BindAttribLocation);     
    OPENGLMEMBERFUNCTION(GetUniformLocation);     
    OPENGLMEMBERFUNCTION(Uniform4f);              
    OPENGLMEMBERFUNCTION(Uniform4fv);             
    OPENGLMEMBERFUNCTION(DeleteVertexArrays);     
    OPENGLMEMBERFUNCTION(EnableVertexAttribArray);
    OPENGLMEMBERFUNCTION(VertexAttribPointer);    
    OPENGLMEMBERFUNCTION(Uniform1i);              
    OPENGLMEMBERFUNCTION(Uniform1f);              
    OPENGLMEMBERFUNCTION(GenerateMipmap);         
    OPENGLMEMBERFUNCTION(UniformMatrix4fv);       
    // Orignal function calls that come with gl/gl.h
    glBindTexture_* glBindTexture;       
};

void RenderTriangleGL(Game_Input *input, Opengl *opengl)
{
    // Get the shader program
    if (opengl->shaderProgram == 0)
    {
        // Init opengl data struct
        opengl->vshadersrc =  "#version 460 core\n"
        "layout (location = 0) in vec3 apos;\n"
        "layout (location = 1) in vec3 acolor;\n"
        "layout (location = 2) in vec2 atexpos;\n"
        "out vec3 newcolor;\n"
        "out vec2 newtexpos;\n"
        "uniform mat4 model;\n"
        "uniform mat4 view;\n"
        "uniform mat4 projection;\n"
        "void main()\n"
        "{\n"
            "gl_Position =  projection * view * model * vec4(apos, 1.0);\n"
            "newcolor = acolor;\n"
            "newtexpos = atexpos;\n"
        "}\n\0";

        opengl->fshadersrc = "#version 460 core\n"
        "out vec4 fragcolor;\n"
        "in vec3 newcolor;\n"
        "in vec2 newtexpos;\n"
        "uniform sampler2D newtex;\n"
        "void main()\n"
        "{\n"
            "vec4 texcolor = texture(newtex, newtexpos);\n"
            "if(texcolor.a < 0.1)\n"
            "   discard;\n"
            "fragcolor = texcolor;"
        "}\n\0";

        // coordinate data of a triangle
        float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
        };

        unsigned int indices[] = {  
            0, 1, 3, // first triangle
            1, 2, 3  // second triangle
        };
        for (int i = 0 ; i < sizeof(vertices)/sizeof(f32); i++)
        {
            opengl->vertices[i] = vertices[i];
        }
        // copy data to the 2d texture buffer
        opengl->data = stbi_load("../bin/textures/asteroids/spaceship.png", &opengl->width, &opengl->height, &opengl->nrchannels, 0); 
        if (opengl->data == NULL)
        {
            OutputDebugString("can't load texture image");
        }

        glGenTextures(1, &opengl->texture);

        // output glerrors
        opengl->glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, opengl->texture);
        // set the texture wrapping/filtering options (on the currently bound texture object)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // loads in texture image
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, opengl->width, opengl->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, opengl->data);
        opengl->glGenerateMipmap(GL_TEXTURE_2D);
        // free image data
        stbi_image_free(opengl->data);
        // vertex shader
        opengl->vertexshader = opengl->glCreateShader(GL_VERTEX_SHADER);
        opengl->glShaderSource(opengl->vertexshader, 1, &opengl->vshadersrc, 0);
        opengl->glCompileShader(opengl->vertexshader);

        // vertex shader compile error
        opengl->glGetShaderiv(opengl->vertexshader, GL_COMPILE_STATUS, &opengl->success);

        if (!opengl->success)
        {
            opengl->glGetShaderInfoLog(opengl->vertexshader, 512, NULL, opengl->infolog);
            OutputDebugString(opengl->infolog);
        }

        // fragment shader
        opengl->fragmentshader = opengl->glCreateShader(GL_FRAGMENT_SHADER);
        opengl->glShaderSource(opengl->fragmentshader, 1, &opengl->fshadersrc, 0);
        opengl->glCompileShader(opengl->fragmentshader);

        // output fragment shader compile errors
        opengl->glGetShaderiv(opengl->fragmentshader, GL_COMPILE_STATUS, &opengl->success);

        if (!opengl->success)
        {
            opengl->glGetShaderInfoLog(opengl->fragmentshader, 512, NULL, opengl->infolog);
            OutputDebugString(opengl->infolog);
        }

        // shader program
        opengl->shaderProgram = opengl->glCreateProgram();
        opengl->glAttachShader(opengl->shaderProgram, opengl->vertexshader);
        opengl->glAttachShader(opengl->shaderProgram, opengl->fragmentshader);
        opengl->glLinkProgram(opengl->shaderProgram);
        opengl->glUseProgram(opengl->shaderProgram);
        // output link errors
        opengl->glGetShaderiv(opengl->shaderProgram, GL_COMPILE_STATUS, &opengl->success);
        if (!opengl->success)
        {
            opengl->glGetShaderInfoLog(opengl->shaderProgram, 512, NULL, opengl->infolog);
            OutputDebugString(opengl->infolog);
        }

        // configure vertex atrribute with vao and vbo
        // TODO: Put ebo in opengl
        opengl->glGenVertexArrays(1, &opengl->vao); 
        opengl->glBindVertexArray(opengl->vao);
        opengl->glGenBuffers(1, &opengl->vbo);
        opengl->glGenBuffers(1, &opengl->ebo);

        opengl->glBindBuffer(GL_ARRAY_BUFFER, opengl->vbo);
        opengl->glBufferData(GL_ARRAY_BUFFER, (void *)(sizeof(opengl->vertices)), opengl->vertices, GL_STATIC_DRAW);

        opengl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, opengl->ebo);
        opengl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, (void *)(sizeof(indices)), indices, GL_STATIC_DRAW);

        opengl->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(f32), (void*)0);
        opengl->glEnableVertexAttribArray(0); 

        opengl->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(f32), (void*)(sizeof(f32) * 3));
        opengl->glEnableVertexAttribArray(1); 

        opengl->glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(f32), (void*)(sizeof(f32) * 6));
        opengl->glEnableVertexAttribArray(2); 

        // unbind vbo and vao after setting vertex atrributes
        opengl->glDeleteShader(opengl->vertexshader);
        opengl->glDeleteShader(opengl->fragmentshader);
    }

    //Render the Triangle
    // local model
    m4x4 model = M4x4(); 
    model = M4x4RotateZ(model, input->degreesTotal);
    // world view 
    m4x4 view = M4x4(); 
    vec4 objPos = Vec4(input->xPos, input->yPos, -2.5);
    view = M4x4Translate(view, objPos);
    // projection mat
    f32 aspectRatio = (f32)input->window_width / input->window_height;
    // projection
    m4x4 projection = M4x4();
    projection = Perspective(120.0, aspectRatio, 100.0, 0.1);
    // clip view
    i32 m4model = opengl->glGetUniformLocation(opengl->shaderProgram, "model");	
    i32 m4view = opengl->glGetUniformLocation(opengl->shaderProgram, "view");	
    i32 m4projection = opengl->glGetUniformLocation(opengl->shaderProgram, "projection");	
    opengl->glUniformMatrix4fv(m4model, 1, GL_TRUE, (const float*)model.m);
    opengl->glUniformMatrix4fv(m4view, 1, GL_TRUE, (const float*)view.m);
    opengl->glUniformMatrix4fv(m4projection, 1, GL_TRUE, (const float*)projection.m);
    glClearColor(0.9f, 0.5f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    SwapBuffers(wglGetCurrentDC());
}


