// @UPDATE: Macro for compiling multiple platforms. 
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <stdint.h>
#include <emmintrin.h>
#include "math.h"

typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float    f32;
typedef double   f64;
typedef size_t   mem_size;

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_JPEG
#define STBI_ONLY_BMP
#define STBI_ONLY_PNG
#include "stb_image.h"

// Game includes.
#define STB_RECT_PACK_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_rect_pack.h"
#include "stb_truetype.h"

// OPENGL THINGS
typedef char     GLchar;
typedef void*    GLsizeiptr;

#define Assert(exp) if ((exp) != true) *(s32 *)0 = 0;
#define global static

#define PushStructScratch(memory, type) (type *) PushMemoryScratch_(memory, sizeof(type))
#define PushArrayScratch(memory, type, count) (type *) PushMemoryScratch_(memory, sizeof(type) * count)
#define PushStruct(memory, type) (type *) PushGameMemory_(memory, sizeof(type))
#define PushArray(memory, type, count) (type *) PushGameMemory_(memory, sizeof(type) * count)

struct File {
    char *data;
    void *handle;
    char *name;

    s32 size;

    // We are only using these values to compare them to old values to see if file has been changed or not.
    s32 prev_write_low;
    s32 prev_write_high;

    s32 last_write_low;
    s32 last_write_high;
};

struct Scratch_Memory {
    void *base;
    void *current;

    s32 size;
    s32 used;
    s32 scratch_count;
};

static void * PushMemoryScratch_(Scratch_Memory *memory, s32 size) {
    Assert(memory->scratch_count > 0);
    Assert(memory->used + size <= memory->size);
    void *memory_address = memory->current;
    memory->used += size;
    memory->current = (u8 *)memory->current + size;
    return memory_address;
}

static void EndScratch(Scratch_Memory *memory) {
    memory->scratch_count--;
    Assert(memory->scratch_count == 0);

    memory->used = 0;
    memory->current = memory->base;
}

struct Game_Memory {
    void *base;
    void *current;
    s32 size;
    s32 used;

    Scratch_Memory scratch;
};

static Scratch_Memory * BeginGameScratch(Game_Memory *memory) {
    Scratch_Memory *scratch = &memory->scratch;
    scratch->scratch_count++;
    return scratch;
}

static void * PushGameMemory_(Game_Memory *memory, s32 size) {
    Assert(memory->used + size <= memory->size);
    void *memory_address = memory->current;
    memory->used += size;
    memory->current = (u8 *)memory->current + size;
    return memory_address;
}

// Things that platform feeds to game.
struct Input {
    void *hwnd;
    f32 x_offset_in_meters, y_offset_in_meters, z_offset_in_meters;
    // Mouse
    s16 mouse_x, mouse_y;
    s16 last_mouse_x, last_mouse_y;
    s16 scroll_amount;

    enum {KEY_DEFAULT, KEY_DOWN, KEY_UP} Key_State;
    enum {LM_DEFAULT, LM_DOWN, LM_UP} LM_State;
    enum {RM_DEFAULT, RM_DOWN, RM_UP} RM_State;

    s32 key_state;
    s32 lm_state;
    s32 rm_state;

    bool is_scrolling;
    bool first_click;
    // Keyboard
    char key_char;

    s32 window_width, window_height;
    bool is_game_running;
    bool is_game_initialized;
}; 

struct Performance_Counter {
    s64 start, end, frequency;
    f32 sec;
    s32 cycle;
};

// These game functions call the platform code.
#define RENDERSCREEN(name) void name(s32 window_width, s32 window_height, s32 width, s32 height, u8 *data) 
typedef RENDERSCREEN(RenderScreen_);

#define SLEEP(name) void name(s32 delta_time) 
typedef SLEEP(Sleep_);

#define READ_ENTIRE_FILE(name) void name(File* file)
typedef READ_ENTIRE_FILE(ReadEntireFile_);

#define OPEN_FILE(name) void name(File* file)
typedef OPEN_FILE(OpenFile_);

#define WRITE_FILE(name) void name(File* file, char *str, s32 size)
typedef WRITE_FILE(WriteFile_);

#define CLOSE_FILE(name) void name(File* file)
typedef CLOSE_FILE(CloseFile_);

#define REMOVE_FILE(name) void name(char* file_name)
typedef REMOVE_FILE(RemoveFile_);

#define GET_FILETIME(name) void name(File *file)
typedef GET_FILETIME(GetFileTime_);

#define STARTCOUNT(name) void name(Performance_Counter *counter)
typedef STARTCOUNT(StartCount_);

#define ENDCOUNT(name) f32 name(Performance_Counter *counter)
typedef ENDCOUNT(EndCount_);

// Function calls from game code to platform.
struct Platform {
    RenderScreen_   *RenderScreen;
    OpenFile_       *OpenFile;
    CloseFile_      *CloseFile;
    WriteFile_      *WriteFile;
    ReadEntireFile_ *ReadEntireFile;
    RemoveFile_     *RemoveFile;
    GetFileTime_    *GetFileTime;
    Sleep_          *Sleep;
    StartCount_     *StartCount;
    EndCount_       *EndCount;
};

// Platform type is shared between game & platform layer.
#define UPDATEGAME(name) void name(Platform *platform, Game_Memory *game_memory, Input *input) 
typedef UPDATEGAME(UpdateGame_);
// For platform to call to update.
UPDATEGAME(UpdateGameStub) {
}
