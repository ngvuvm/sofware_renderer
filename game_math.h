/* MATRIX MATHS FOR TRANSFORMATION */
#define PI  3.14159265f
#define TAU 6.28318530f

// vector 4 components
// TODO: Remove v as unnecessary, using operator overloading to access the values instead.
// TODO(critical): a lot of v4 operator overloadings are done for other misc ray trace code that are not correct for v4, only for vec3.
struct v2
{
    union
    {
        struct 
        {
            f32 x;
            f32 y;
        };
        f32 e[2]; 
    };
};

struct v3
{
    union
    {
        struct 
        {
            f32 x;
            f32 y;
            f32 z;
        };
        f32 e[3]; 
    };
};

struct v4
{
    union
    {
        struct 
        {
            f32 x;
            f32 y;
            f32 z;
            f32 w;
        };
        f32 e[4]; 
    };
};

struct v4i
{
    union
    {
        struct 
        {
            s32 x;
            s32 y;
            s32 z;
            s32 w;
        };
        s32 e[4]; 
    };
};

// Matrix 4x4
struct m4x4 
{
    f32 m[16];
};

// matrix notation: column major notations
// overload matrix operation
// initialize matrix
m4x4 M4x4() 
{
    m4x4 mat = 
    {
        {1.0f, 0.0f, 0.0f, 0.0f,
         0.0f, 1.0f, 0.0f, 0.0f,
         0.0f, 0.0f, 1.0f, 0.0f,
         0.0f, 0.0f, 0.0f, 1.0f}
    };
    return mat; 
}
// Vector constructors.
v2 Vec2()
{
    return {0, 0};
}

// Vector operator overloadings.
v3 operator-(v3 a, v3 b)
{
    a.x = a.x - b.x;
    a.y = a.y - b.y;
    a.z = a.z - b.z;
    return a;
}

v4 Vec4()
{
    return {0, 0, 0 , 1};
}

v4 Vec4(f32 x, f32 y, f32 z)
{
    return {x, y, z , 1};
}


v4 operator-(v4 a, v4 b)
{
    a.x = a.x - b.x;
    a.y = a.y - b.y;
    a.z = a.z - b.z;
    a.w = a.w - b.w;

    return a;
}

v4 operator+(v4 a, v4 b)
{
    a.x = a.x + b.x;
    a.y = a.y + b.y;
    a.z = a.z + b.z;
    a.z = a.w + b.w;

    return a;
}

v4 operator*(v4 a, f32 b)
{
    a.x = a.x * b;
    a.y = a.y * b;
    a.z = a.z * b;
    a.w = a.w * b;

    return a;
}

v4 operator/(v4 a, f32 b)
{
    a.x = a.x / b;
    a.y = a.y / b;
    a.z = a.z / b;
    a.w = a.w / b;

    return a;
}

v4i V4i()
{
    return {0, 0, 0, 0};
}

v4i V4i(s32 x, s32 y, s32 z)
{
    return {x, y, z , 0};
}


v4i operator-(v4i a, v4i b)
{
    a.x = a.x - b.x;
    a.y = a.y - b.y;
    a.z = a.z - b.z;
    a.w = a.w - b.w;

    return a;
}

v4i operator+(v4i a, v4i b)
{
    a.x = a.x + b.x;
    a.y = a.y + b.y;
    a.z = a.z + b.z;
    a.z = a.w + b.w;

    return a;
}

v4i operator*(v4i a, s32 b)
{
    a.x = a.x * b;
    a.y = a.y * b;
    a.z = a.z * b;
    a.w = a.w * b;

    return a;
}

v4i operator/(v4i a, s32 b)
{
    a.x = a.x / b;
    a.y = a.y / b;
    a.z = a.z / b;
    a.w = a.w / b;

    return a;
}

//TODO: Not actually math, confusing
v4 operator*(v4 a, v4 b)
{
    a.x = a.x * b.x;
    a.y = a.y * b.y;
    a.z = a.z * b.z;
    a.w = a.w * b.w;

    return a;
}

//TODO: Not actually math, confusing
v4 operator/(v4 a, v4 b)
{
    a.x = a.x / b.x;
    a.y = a.y / b.y;
    a.z = a.z / b.z;
    a.w = a.w / b.w;

    return a;
}

// MATRIX MULTIPLICATIONS
// m4x4 * scalar
m4x4 operator*(m4x4 b, f32 a)
{
    m4x4 m = {};

    for(int r = 0; r < 16; r++)
    {
        m.m[r] = b.m[r] * a;
    }

    return m;
}

// m4x4 * v4
v4 operator*(m4x4 b, v4 a)
{
    v4 m = {};
    int row = 0;

    for(int r = 0; r < 4; r++)
    {
        m.e[r] = b.m[row]   * a.e[0] +
                 b.m[row+1] * a.e[1] + 
                 b.m[row+2] * a.e[2] + 
                 b.m[row+3] * a.e[3]; 
        row += 4;
    }

    return m;
}

// m4x4 * m4x4
m4x4 operator*(m4x4 a, m4x4 y)
{
    m4x4 m = {};
    int row = 0;
    int column = 0;

    for(int r = 0; r < 16; r++)
    {
        m.m[r] = a.m[column]   * y.m[row]   +
                 a.m[column+1] * y.m[row+4] + 
                 a.m[column+2] * y.m[row+8] + 
                 a.m[column+3] * y.m[row+12]; 

        if (row == 3)
        {
            row = 0;
            column += 4;
        }
        else 
        {
            row += 1;
        }
    }

    return m;
}

m4x4 M4x4Transpose(m4x4 a)
{
    m4x4 r = {};

    r.m[0]  = a.m[0];
    r.m[1]  = a.m[4];
    r.m[2]  = a.m[8];
    r.m[3]  = a.m[12];

    r.m[4]  = a.m[1];
    r.m[5]  = a.m[5];
    r.m[6]  = a.m[9];
    r.m[7]  = a.m[13];

    r.m[8]  = a.m[2];
    r.m[9]  = a.m[6];
    r.m[10] = a.m[10];
    r.m[11] = a.m[14];

    r.m[12] = a.m[3];
    r.m[13] = a.m[7];
    r.m[14] = a.m[11];
    r.m[15] = a.m[15];

    return r;
}

f32 M3x3Determinant(f32 e0, f32 e1, f32 e2, f32 e3, f32 e4, f32 e5, f32 e6, f32 e7, f32 e8)
{
    // e0 e1 e2
    // e3 e4 e5
    // e6 e7 e8

    return e0 * (e4 * e8 - e5 * e7) - e1 * (e3 * e8 - e5 * e6) + e2 * (e3 * e7 - e4 * e6);
}

m4x4 M4x4Inverse(m4x4 a)
{
    m4x4 r = {};

    f32 e0  = a.m[0];
    f32 e1  = a.m[1];
    f32 e2  = a.m[2];
    f32 e3  = a.m[3];

    f32 e4  = a.m[4];
    f32 e5  = a.m[5];
    f32 e6  = a.m[6];
    f32 e7  = a.m[7];

    f32 e8  = a.m[8];
    f32 e9  = a.m[9];
    f32 e10 = a.m[10];
    f32 e11 = a.m[11];

    f32 e12 = a.m[12];
    f32 e13 = a.m[13];
    f32 e14 = a.m[14];
    f32 e15 = a.m[15];
    
    // Matrix of minors
    // e0  e1  e2  e3
    // e4  e5  e6  e7
    // e8  e9  e10 e11
    // e12 e13 e14 e15
    //
    // d0  d1  d2  d3
    // d4  d5  d6  d7
    // d8  d9  d10 d11
    // d12 d13 d14 d15
    //
    // d0
    m4x4 d;
    d.m[0]  =  M3x3Determinant(e5, e6, e7, e9, e10, e11, e13, e14, e15);
    d.m[1]  = -M3x3Determinant(e4, e6, e7, e8, e10, e11, e12, e14, e15);
    d.m[2]  =  M3x3Determinant(e4, e5, e7, e8, e9,  e11, e12, e13, e15);
    d.m[3]  = -M3x3Determinant(e4, e5, e6, e8, e9,  e10, e12, e13, e14);

    d.m[4]  = -M3x3Determinant(e1, e2, e3, e9, e10, e11, e13, e14, e15);
    d.m[5]  =  M3x3Determinant(e0, e2, e3, e8, e10, e11, e12, e14, e15);
    d.m[6]  = -M3x3Determinant(e0, e1, e3, e8, e9,  e11, e12, e13, e15);
    d.m[7]  =  M3x3Determinant(e0, e1, e2, e8, e9,  e10, e12, e13, e14);

    d.m[8]  =  M3x3Determinant(e1, e2, e3, e5, e6, e7, e13, e14, e15);
    d.m[9]  = -M3x3Determinant(e0, e2, e3, e4, e6, e7, e12, e14, e15);
    d.m[10] =  M3x3Determinant(e0, e1, e3, e4, e5, e7, e12, e13, e15);
    d.m[11] = -M3x3Determinant(e0, e1, e2, e4, e5, e6, e12, e13, e14);

    d.m[12] = -M3x3Determinant(e1, e2, e3, e5, e6, e7, e9, e10, e11);
    d.m[13] =  M3x3Determinant(e0, e2, e3, e4, e6, e7, e8, e10, e11);
    d.m[14] = -M3x3Determinant(e0, e1, e3, e4, e5, e7, e8,  e9, e11);
    d.m[15] =  M3x3Determinant(e0, e1, e2, e4, e5, e6, e8,  e9, e10);

    m4x4 d_t = M4x4Transpose(d);

    f32 one_over_d = 1 / (e0 * d.m[0] + e1 * d.m[1] + e2 * d.m[2] + e3 * d.m[3]);
    // Need Assert over here
    // Assert(d != 0);

    r = d_t * one_over_d;
    return r;
}

m4x4 M4x4Translate(f32 x, f32 y, f32 z) 
{
    m4x4 r = M4x4();

    r.m[3] = x;
    r.m[7] = y;
    r.m[11] = z;

    return r;
}

// SCALING
m4x4 M4x4Scale(f32 x, f32 y, f32 z) 
{
    m4x4 r  = M4x4();
    r.m[0]  = x;
    r.m[5]  = y;
    r.m[10] = z;
    return r;
}

// ROTATE
m4x4 M4x4RotateX(f32 d)
{
    f32 radian = d * PI/180;
    m4x4 r  = M4x4();
    r.m[5]  = (f32)cos(radian);
    r.m[6]  = (f32)-sin(radian);
    r.m[9]  = (f32)sin(radian);
    r.m[10] = (f32)cos(radian);
    return r;
}

m4x4 M4x4RotateY(f32 d)
{
    f32 radian = d * PI/180;
    m4x4 r  =  M4x4();
    r.m[0]  =  (f32)cos(radian);
    r.m[2]  =  (f32)sin(radian);
    r.m[8]  =  (f32)-sin(radian);
    r.m[10] =  (f32)cos(radian);
    return r;
}
m4x4 M4x4RotateZ(f32 d)
{
    f32 radian = d * PI/180;
    f32 sine   = f32(sin(radian));
    f32 cosine = f32(cos(radian));
    m4x4 r =  M4x4();
    r.m[0] =  cosine;
    r.m[1] = -sine;
    r.m[4] =  sine;
    r.m[5] =  cosine;
    return r;
}

m4x4 M4x4Perspective(f32 fov, f32 aspect_ratio, f32 f, f32 n)
{
    m4x4 p = M4x4();
    f32 rad_fov = (fov/2) * (PI/180);
    f32 scale = (f32)tan(rad_fov) * n;
    f32 r = aspect_ratio * scale;
    f32 l = -r;
    f32 t = scale;
    f32 b = -t;

    // first col
    p.m[0]  = 2.0f * n/(r - l);
    p.m[4]  = 0.0f;
    p.m[8]  = 0.0f;
    p.m[12] = 0.0f;
    // second col
    p.m[1]  = 0.0f;
    p.m[5]  = 2.0f * n/(t - b);
    p.m[9]  = 0.0f;
    p.m[13] = 0.0f;
    // third col
    p.m[2]  = (r + l)/(r - l);
    p.m[6]  = (t + b)/(t - b);
    //p.m[10] = -(f + n)/(f-n);
    p.m[10] = n/(f-n); // Swap near and far mapping
    p.m[14] = -1.0f;
    // fourth col
    p.m[3]  =  0.0f;
    p.m[7]  =  0.0f;
    //p.m[11] = -2*f*n/(f-n);
    p.m[11] = f*n/(f-n); // Swap near and far mapping
    p.m[15] =  1.0f;

    return p;
}

m4x4 M4x4Orthographic(f32 scale, f32 aspect_ratio, f32 f, f32 n)
{
    m4x4 p = M4x4();

    f32 r = aspect_ratio * scale;
    f32 l = -r;
    f32 t = scale;
    f32 b = -t;

    // first col
    p.m[0]  = 2.0f/(r - l);
    p.m[4]  = 0.0f;
    p.m[8]  = 0.0f;
    p.m[12] = 0.0f;
    // second col
    p.m[1]  = 0.0f;
    p.m[5]  = 2.0f/(t - b);
    p.m[9]  = 0.0f;
    p.m[13] = 0.0f;
    // third col
    p.m[2]  = 0.0f;
    p.m[6]  = 0.0f;
    p.m[10] = n/(f - n);
    p.m[14] = 0.0f;
    // fourth col
    p.m[3]  = -(r + l)/(r - l);
    p.m[7]  = -(t + b)/(t - b);
    p.m[11] = f*n/(f-n);
    p.m[15] = 1.0f;

    return p;
}

m4x4 M4x4ViewPort(s32 x, s32 y, s32 w, s32 h) 
{
    m4x4 m = M4x4();
    m.m[3] = x+w/2.f;
    m.m[7] = y+h/2.f;

    m.m[0] = w/2.f;
    m.m[5] = h/2.f;

    return m;
}

// Utilities
f32 Dot(v4 a, v4 b)
{
     return a.x * b.x + a.y * b.y + a.z * b.z;
}

v4 Cross(v4 a, v4 b)
{
    v4 c = {};
    c.x = a.y * b.z - a.z * b.y;
    c.y = a.z * b.x - a.x * b.z;
    c.z = a.x * b.y - a.y * b.x;
    return c;
}

f32 LengthV4(v4 v)
{
    return (f32)sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

s32 LengthV4i(v4i v)
{
    return (s32)sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

v4 Normalize(v4 v)
{
    v4 r = v/LengthV4(v);
    r.w = v.w;

    return r;
}

f32 Lerp(f32 a, f32 b, f32 t)
{
    return (1 - t) *a + t * b;
}

v4 Lerp(v4 a, v4 b, f32 t)
{
    return a * (1 - t) + b * t;
}

static v4 ExtractBasisRow(m4x4 m, s32 row_index)
{
    return {m.m[row_index * 4], m.m[row_index * 4 + 1], m.m[row_index * 4 + 2]};
}

static v4 ExtractMatrixColumn(m4x4 m, s32 col_index)
{
    f32 e0 = m.m[0 * 4 + col_index];
    f32 e1 = m.m[1 * 4 + col_index];
    f32 e2 = m.m[2 * 4 + col_index];
    f32 e3 = m.m[3 * 4 + col_index];
    return {e0, e1, e2, e3};
}

s32 GetMin(s32 a, s32 b, s32 c)
{
    a = a < b ? a : b;
    a = a < c ? a : c;
    return a;
}

s32 GetMax(s32 a, s32 b, s32 c)
{
    a = a > b ? a : b;
    a = a > c ? a : c;
    return a;
}

// TODO: Replace floor from standard lib.
f64 GetMinFloor(f32 a, f32 b, f32 c)
{
    a = a < b ? a : b;
    a = a < c ? a : c;
    return floor(a);
}

// TODO: Replace ceil from standard lib.
f64 GetMaxCeil(f32 a, f32 b, f32 c)
{
    a = a > b ? a : b;
    a = a > c ? a : c;
    return ceil(a);
}

f32 EdgeTest(v4 a, v4 b, v4 c)
{
    return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x));
}

s32 EdgeTestInt(v4i a, v4i b, v4i c)
{
    return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x));
}

v4 GetBaryCoords(v4 a, v4 b, v4 c, v4 p)
{
    v4 v0 = b - a;
    v4 v1 = c - a;
    v4 v2 = p - a;
    
    f32 dot00 = Dot(v0, v0);
    f32 dot01 = Dot(v0, v1);
    f32 dot02 = Dot(v0, v2);
    f32 dot11 = Dot(v1, v1);
    f32 dot12 = Dot(v1, v2);
    
    f32 u = (dot11 * dot02 - dot01 * dot12) / (dot00 * dot11 - dot01 * dot01);
    f32 v = (dot00 * dot12 - dot01 * dot02) / (dot00 * dot11 - dot01 * dot01);
    
    v4 result = {(1 - u - v), u, v, 0};
    return result;
}
