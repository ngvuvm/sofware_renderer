struct Tile
{
    v3 position;
    m4x4 to_visual_position;
    bool is_occupied;
};

struct Tile_Map
{
    Tile *tiles;
    s32 tile_width, tile_height, row_count, col_count, tile_count;
};
