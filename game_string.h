// TODO: Work to include this if need be.
// General purpose string library.
#include "stdio.h"

#define STRING(str) CreateString(str)
#define PSTRING(str) GetStringPointer(CreateString(str))
#define NSTRING(len) CreateString(len)
#define PNSTRING(len) GetStringPointer(CreateString(len))
#define PNSTRING_EX(str,len) GetStringPointer(CreateString(str, len))

static Game_Memory *string_mem_region;

void SetStringMemory(Memory_Region* mem_region)
{
    string_mem_region = mem_region;
}

// Never use the data directly. Always go through the ultility functions.
struct String
{
    char* data;
    i32 length;
};

struct String_Array
{
    String *strings;
    i32 length;
    i32 count;
    i32 capacity;

    String operator[](i32 i) { return strings[i];};
};

// Only use this when you know that the string is properly null terminated.
// This is mostly for iternal usage.
mem_size RawStringLength(char* data)
{
    i32 length = 0;

    while (data[length] != '\0')
    {
        length++;
    }

    return length;
}

String CreateString(char* src)
{
    String str = {};
    str.length = RawStringLength(src);

    char *data = (char *)MemStackAlloc(string_mem_region, str.length);
    str.data = data;
    memcpy(str.data, src, str.length);
    return str;
}

String CreateString(i32 length)
{
    String str = {};
    str.length = length;

    char *data = (char *)MemStackAlloc(string_mem_region, str.length);
    str.data = data;
    return str;
}

String CreateString(char* src, i32 length)
{
    String str = {};
    str.length = length;

    char *data = (char *)MemStackAlloc(string_mem_region, str.length);
    str.data = data;
    memcpy(str.data, src, str.length);
    return str;
}

String SubString(String *source_str, i32 start, i32 end)
{
    Assert (end >= start);
    Assert (source_str->length >= (end - start));

    String dest_str = CreateString(end - start);
    memcpy(dest_str.data, source_str->data + start, end - start);

    return dest_str;
}

// Not a great ideal in general if you work with C style null-terminated string.
String KillNullsInStringCopy(String *str)
{
    i32 real_length = 0;
    for (i32 i = 0; i < str->length; ++i)
    {
        if (str->data[i] == '\0')
        {
            
        }
        else
        {
            real_length++;
        }
    } 
   
    String new_str = NSTRING(real_length); 

    for (i32 i = 0, j = 0; i < str->length; ++i)
    {
        if (str->data[i] != '\0')
        {
            new_str.data[j] = str->data[i];
            j++;
        }
    } 
     
    return new_str;
}

bool ValidateString(String *string)
{
    bool result = string->data ? 1 : 0;
    return result;
}

void NullifyString(String *str)
{
    for (i32 i = 0; i < str->length; ++i)
    {
        str->data[i] = '\0';
    }
}

// When this joins strings inside the string array of the String_Array structure, every leftover nulls inside a String data
// will be ignore and the computed length of the new string will get a minus 1. 
String JoinStringArray(String_Array *str_array, char delimiter)
{ 
    Assert(str_array->length && str_array->count);
    String a = {};
    a.length = str_array->length + str_array->count - 1; // Plus a space for each delimiter, then minus one for last position
    a = CreateString(a.length);

    i32 a_index = 0;
    for (i32 i = 0; i < str_array->count; ++i)
    {
        String *str = &str_array->strings[i];
        for (i32 j = 0; j < str->length; ++j)
        {
            a.data[a_index] = str->data[j];
            a_index++;
        }

        if (i != str_array->count - 1)
        {
            a.data[a_index] = delimiter;
            a_index++;
        }
    }

    return a;
}

String ConcatString(String *a, String *b)
{ 
    i32 total_length = 0;
    total_length = a->length + b->length;

    String result = CreateString(total_length);
    
    i32 string_index = 0;
    for (i32 i = 0; i < a->length; ++i)
    {
        result.data[string_index++] = a->data[i];
    }

    for (i32 i = 0; i < b->length; ++i)
    {
        result.data[string_index++] = b->data[i];
    }

    return result;
}

String ConcatString(String *a, String *b, String *c)
{ 
    i32 total_length = 0;
    total_length = a->length + b->length + c->length;

    String result = CreateString(total_length);
    i32 string_index = 0;

    for (i32 i = 0; i < a->length; ++i)
    {
        result.data[string_index++] = a->data[i];
    }

    for (i32 i = 0; i < b->length; ++i)
    {
        result.data[string_index++] = b->data[i];
    }

    for (i32 i = 0; i < c->length; ++i)
    {
        result.data[string_index++] = c->data[i];
    }

    return result;
}

String ConcatString(String *a, String *b, String *c, String *d)
{ 
    i32 total_length = 0;
    total_length = a->length + b->length + c->length;

    String result = CreateString(total_length);
    i32 string_index = 0;

    for (i32 i = 0; i < a->length; ++i)
    {
        result.data[string_index++] = a->data[i];
    }

    for (i32 i = 0; i < b->length; ++i)
    {
        result.data[string_index++] = b->data[i];
    }

    for (i32 i = 0; i < c->length; ++i)
    {
        result.data[string_index++] = c->data[i];
    }

    for (i32 i = 0; i < d->length; ++i)
    {
        result.data[string_index++] = d->data[i];
    }

    return result;
}

// Direct c char versions 
String ConcatString(char* a, char* b)
{ 
    i32 total_length = 0;
    i32 length_a = RawStringLength(a);
    i32 length_b = RawStringLength(b);
    total_length = length_a + length_b;

    String result = CreateString(total_length);
    i32 string_index = 0;
    
    for (i32 i = 0; i < length_a; ++i)
    {
        result.data[string_index++] = a[i];
    }

    for (i32 i = 0; i < length_b; ++i)
    {
        result.data[string_index++] = b[i];
    }

    return result;
}

String ConcatString(char* a, char* b, char* c)
{ 
    i32 total_length = 0;
    i32 length_a = RawStringLength(a);
    i32 length_b = RawStringLength(b);
    i32 length_c = RawStringLength(c);
    total_length = length_a + length_b + length_c;

    String result = CreateString(total_length);
    i32 string_index = 0;

    for (i32 i = 0; i < length_a; ++i)
    {
        result.data[string_index++] = a[i];
    }

    for (i32 i = 0; i < length_b; ++i)
    {
        result.data[string_index++] = b[i];
    }

    for (i32 i = 0; i < length_c; ++i)
    {
        result.data[string_index++] = c[i];
    }

    return result;
}

String ConcatString(char *a, char *b, char *c, char *d)
{ 
    i32 total_length = 0;
    i32 length_a = RawStringLength(a);
    i32 length_b = RawStringLength(b);
    i32 length_c = RawStringLength(c);
    i32 length_d = RawStringLength(d);
    total_length = length_a + length_b + length_c + length_d;

    String result = CreateString(total_length);
    i32 string_index = 0;

    for (i32 i = 0; i < length_a; ++i)
    {
        result.data[string_index++] = a[i];
    }

    for (i32 i = 0; i < length_b; ++i)
    {
        result.data[string_index++] = b[i];
    }

    for (i32 i = 0; i < length_c; ++i)
    {
        result.data[string_index++] = c[i];
    }

    for (i32 i = 0; i < length_d; ++i)
    {
        result.data[string_index++] = d[i];
    }

    return result;
}

String SurroundString(char a, String *str, i32 length)
{
    i32 str_length = length;
    String result_str = CreateString(str_length + 2);
    result_str.data[0] = a;
    result_str.data[result_str.length - 1] = a;
    memcpy(result_str.data + 1, str->data, str_length);

    return result_str;
}

char * GetCString(String *a)
{
    char *c_str = (char *)MemStackAlloc(string_mem_region, a->length + 1); // Add 1 for null char.

    for (i32 i = 0; i < a->length; ++i)
    {
        c_str[i] = a->data[i];
    }

    c_str[a->length] = '\0';
    return c_str;
}

String_Array CreateStringArray(i32 capacity)
{
    String_Array str_arr = {};
    str_arr.capacity = capacity;
    str_arr.strings = (String *)MemStackAlloc(string_mem_region, str_arr.capacity * sizeof(String));

    return str_arr;
}

void PushStringToArray(String *str, String_Array *arr)
{
    // Guard the array push
    if (arr->count < arr->capacity)
    {
        arr->strings[arr->count] = *str;
        arr->count++;
        arr->length += str->length;
    }
    else
    {
        // TODO: Handle non crash error loggings
    }
}

bool StringNullCheck(String* str)
{
    bool result = false;
    if (str->data[str->length] == '\0')
    {
        result = true;
    }

    return result;
}

String * GetStringPointer(String str)
{
    return &str;
}

void StringCopy(String *dest, String *src)
{
    i32 length = 0;
    if (dest->length <= src->length)
    {
        length = dest->length;
    }
    else
    {
        length = src->length;
    }

    for (i32 i = 0; i < length; ++i)
    {
        dest->data[i] = src->data[i];
    }
}

// This is mainly for copying the string to the permanent region. 
// TODO(CLEANUP): Permanent region and temporary region are of the same Memory_Region type right now. Clean this up!. Generally we'd 
// want to handle types' behaviors differently so it'd provide more clarity to have different types.
String PushStringToRegion(String *str, Memory_Region *memory_region)
{
    Memory_Region *prev_region = string_mem_region;
    
    SetStringMemory(memory_region);
    String result = CreateString(str->data, str->length);

    SetStringMemory(prev_region);
    return result;
}

#ifdef DEBUG
// Max 256 bytes
void DebugPrintString(String s)
{
    char a[256];
    memcpy(a, s.data, s.length);
    printf("%s", a);
}
#endif
