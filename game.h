#include "shared.h"
#include "game_math.h"
#include "game_world.h"
#include "game_render.h"
#include "game_variables.h"
#include "game_ui.h"

enum Game_Mode {PLAY_MODE, EDIT_MODE};

struct Debug_Info
{
    f32 frames_micro, total_sec;
};

struct Selection {
    Mesh **meshes;
    s32 count;
};

struct Unit {
    Mesh *mesh;
    Mesh *target_circle;
    Mesh *collision_circle;

    v4 world_position; // origin position.
};

struct Game_State {
    Tile_Map  *tile_map;
    Game_UI   *game_ui;
    Selection *selection;
    Unit      *unit;

    Game_Mode  mode;
    f32 y_rotate_degrees;
    f32 target_ms;
    f32 t_micro_accumulator;
    f32 t_micro;
    f32 degrees;
    bool is_video_sync;
    bool report_position;
    bool is_moved = true;
};

struct Game {
    Game_Memory *memory; 
    Input       *input;
    Platform    *platform_api;
    Game_State  *game_state; 
    Renderer    *renderer;
    Font        font;

    bool is_initialized;
};

struct Record_Playback 
{
    bool recording;
    bool open_recording;
    bool delete_recording;
};
