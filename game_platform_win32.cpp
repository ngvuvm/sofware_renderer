#include <stdio.h>

#include "shared.h"
#include "game_platform_win32.h"

// Globals
global BITMAPINFO global_bmi;
global HDC hdc;

// TODO(BUG FIX): Full file path, or maybe only file without permissions get crashed. And do better error handling.
static OPEN_FILE(Win32OpenFile)
{
    file->handle = CreateFileA(file->name, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    DWORD error = GetLastError();
    file->size = GetFileSize(file->handle, 0);
    error = GetLastError();
}

static WRITE_FILE(Win32WriteFile)
{
    DWORD bytes_written = 0;
    WriteFile(file->handle, str, size, &bytes_written, 0);
}

static CLOSE_FILE(Win32CloseFile)
{
    CloseHandle(file->handle);
}

static REMOVE_FILE(Win32RemoveFile)
{
    DeleteFileA(file_name);
}

static READ_ENTIRE_FILE(Win32ReadEntireFile)
{
    DWORD bytes_read = 0;
    Assert(ReadFile(file->handle, file->data, (DWORD)file->size, &bytes_read, 0) != 0);
}

static GET_FILETIME(Win32GetFileTime)
{
    FILETIME create_time;
    FILETIME last_access;
    FILETIME last_write;
    if (GetFileTime(file->handle, &create_time, &last_access, &last_write))
    {
        file->last_write_low  = last_write.dwLowDateTime;
        file->last_write_high = last_write.dwHighDateTime;
    }
}

static RENDERSCREEN(Win32RenderScreen)
{
    StretchDIBits(hdc, 0, 0, window_width, window_height, 0, 0, width, height, data, &global_bmi, DIB_RGB_COLORS, SRCCOPY);
}

static SLEEP(Win32Sleep)
{
    Sleep(delta_time);
}

// TODO: Get this done.
// Record and playback is a debug feature that only works with a fixed frame rate, currently not
// working with variable frame rate, since the computed inputs have different delta time
// Fixed framerate setting is by default for development due to its deterministic nature
// static void HandleRecording(Input input, Record_Playback record_playback);

static STARTCOUNT(Win32StartCount)
{
    LARGE_INTEGER start_large = {};
    LARGE_INTEGER frequency_large = {};

    QueryPerformanceFrequency(&frequency_large);
    QueryPerformanceCounter(&start_large);

    counter->frequency = frequency_large.QuadPart;
    counter->start =  start_large.QuadPart;
}

static ENDCOUNT(Win32EndCount)
{
    f32 microseconds_per_frame = 0.0f;

    LARGE_INTEGER end_large = {};
    QueryPerformanceCounter(&end_large);
    counter->end = end_large.QuadPart;

    microseconds_per_frame = (f32)(counter->end - counter->start);
    microseconds_per_frame *= 1000000;
    return microseconds_per_frame /= (f32)(counter->frequency);
}

/*
void LoadGLFunctions(Opengl *opengl)
{
// load opengl32.dll functions into opengl struct function member pointers
#define GL_DCL(ret, name, ...) opengl->gl##name = (gl##name##_ *) wglGetProcAddress("gl"#name);     \
    if (!opengl->gl##name) {                                                                       \
        OutputDebugStringA("Function gl" #name " couldn't be loaded from opengl32.dll\n"); \
    }
    GL_DCL_LIST
// pull existing functions defined in gl/gl.h into opengl struct function member pointers
    if (&glBindTexture)
        opengl->glBindTexture = &glBindTexture;
#undef GL_DCL
}

void LoadOpenGL(HWND hwnd, Opengl* opengl)
{
    // Set device context and openglcontext 
    // TODO: change width/height
    glViewport(0, 0, 1366, 768);
    PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
        PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
        32,                   // Colordepth of the framebuffer.
        0, 0, 0, 0, 0, 0,
        0,
        0,
        0,
        0, 0, 0, 0,
        24,                   // Number of bits for the depthbuffer
        8,                    // Number of bits for the stencilbuffer
        0,                    // Number of Aux buffers in the framebuffer.
        PFD_MAIN_PLANE,
        0,
        0, 0, 0
    };
    opengl->deviceContext = GetDC(hwnd);
    s32 pxFormat;
    pxFormat = ChoosePixelFormat(opengl->deviceContext, &pfd); 
    DescribePixelFormat(opengl->deviceContext, pxFormat, sizeof(pfd), &pfd);
    bool pixelFormatBool = SetPixelFormat(opengl->deviceContext, pxFormat, &pfd);
    opengl->openGLRenderContext = wglCreateContext(opengl->deviceContext); 
    wglMakeCurrent(opengl->deviceContext, opengl->openGLRenderContext);
    
    LoadGLFunctions(opengl);
}
*/

static void HandleInput(Input* input, HWND hwnd, MSG msg)
{
    // Message loop
    bool bRet;
    bRet = PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE);
    if (bRet != false)
    {
        switch (msg.message) 
        {
            case WM_PAINT:
            {
                // Not used
                //PAINTSTRUCT ps;
                //hdc =BeginPaint(hwnd, &ps);
                //EndPaint(hwnd, &ps);
            } break;

            case WM_KEYDOWN:
            {
                input->key_char  = (char)msg.wParam;
                input->key_state = input->KEY_DOWN;
            } break;

            case WM_KEYUP:
            {
                input->key_char  = (char)msg.wParam;
                input->key_state = input->KEY_UP;
            } break;

            case WM_MOUSEWHEEL:
            {
                input->scroll_amount = (s16)(msg.wParam >> 16) / WHEEL_DELTA;
                input->is_scrolling = true;
            } break;

            case WM_RBUTTONDOWN:
            {
                if (input->rm_state == input->RM_DEFAULT) // Only at first mouse right click, not when it's held down.
                {
                    input->last_mouse_x = input->mouse_x;
                    input->last_mouse_y = input->mouse_y;
                }

                // Mouse right click is being held down.
                input->rm_state = input->RM_DOWN;

            } break;

            case WM_RBUTTONUP:
            {
                input->rm_state = input->RM_UP;
            } break;

            case WM_LBUTTONDOWN:
            {
                if (input->lm_state == input->LM_DEFAULT)
                {
                    input->last_mouse_x = input->mouse_x;
                    input->last_mouse_y = input->mouse_y;
                }

                input->lm_state = input->LM_DOWN;

            } break;

            case WM_LBUTTONUP:
            {
                input->lm_state = input->LM_UP;
            } break;

            case WM_MOUSEMOVE:
            {
                input->mouse_x = (s16)(msg.lParam);
                input->mouse_y = (s16)(msg.lParam >> 16);
            } break;

            default:
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            } break;
        }
    }
}

// DLL functions
UpdateGame_* UpdateGame;

static LRESULT CALLBACK WinProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
    switch (uMsg) 
    {
        case WM_ERASEBKGND:
        {
            return 1;
        } break;

        case WM_PAINT:
        {
        } break;
        case WM_CREATE: 
        {
        } break;

        case WM_CLOSE: 
        {
            PostQuitMessage(0);
        }
        break;
 
        default: 
            return DefWindowProc(hwnd, uMsg, wParam, lParam); 
    } 
    return 0; 
} 
 
static void ReloadGameDll(Load_Info_DLL *dll_info, char* src_dll, char* dest_dll, s32 frame_count_to_load)
{
    if (dll_info->load_count++ > frame_count_to_load) // run after a number of frames
    {
        // Load up the temporary/destination dll that we want to get the functions from.
        HANDLE src_dll_file = CreateFileA(src_dll, GENERIC_READ|GENERIC_WRITE, 0, 0, 
                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

        if (GetFileTime(src_dll_file, 0, 0, &dll_info->dllFileTime))
        {
            //OutputDebugStringA("File time retrieved successfully\n");
            const FILETIME* lpDllFileTime     = &dll_info->dllFileTime;
            const FILETIME* lpLastDllFileTime = &dll_info->lastDllFileTime;

            // close right after retrieving to copy
            if(CloseHandle(src_dll_file))
            {
                //OutputDebugStringA("file handle closed successfully\n");
            }
            
            if (CompareFileTime(lpDllFileTime, lpLastDllFileTime) != 0)
            {
                // Point the dll functions to a stub
                UpdateGame = UpdateGameStub;
                // Load file when it's changed since last modified
                // Reload the library after getting the new copy
                FreeLibrary(dll_info->temp_dll);
                CopyFile(src_dll, dest_dll, false);

                dll_info->temp_dll = LoadLibraryA(dest_dll);
                int er = GetLastError();

                UpdateGame = (UpdateGame_ *)(GetProcAddress(dll_info->temp_dll, "UpdateGame"));;
            }

            dll_info->lastDllFileTime.dwLowDateTime = dll_info->dllFileTime.dwLowDateTime;
            dll_info->lastDllFileTime.dwHighDateTime = dll_info->dllFileTime.dwHighDateTime;
        }

        int err = GetLastError();
        dll_info->load_count = 0;
    }
}

// TODO: Get back to this at some point
/*
void HandleRecording(Game_Input* input, Record_Playback* record_playback, char* file_name)
{
    record_playback->snapshot = CreateFileA(file_name, GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    // open recording
    if (record_playback->open_recording == true)
    {
        DWORD bytes_read = 0;
        LARGE_INTEGER file_size = {};
        GetFileSizeEx(record_playback->snapshot, &file_size);
        if (record_playback->file_pointer == file_size.LowPart)
        {
            record_playback->open_recording = false;
            record_playback->file_pointer = 0;
        }

        SetFilePointer(record_playback->snapshot, record_playback->file_pointer, 0, FILE_BEGIN);
        memset(input, 0, sizeof(Game_Input));
        bool success = ReadFile(record_playback->snapshot, input, sizeof(Game_Input), &bytes_read, 0);
        record_playback->file_pointer = SetFilePointer(record_playback->snapshot, 0, 0, FILE_CURRENT);
        OutputDebugStringA("open recording\n");
    }

    // record input
    if (record_playback->recording == true)
    {
        // write into file input for the frame
        DWORD bytes_written = 0;
        SetFilePointer(record_playback->snapshot, 0, 0, FILE_END);
        bool success = WriteFile(record_playback->snapshot, input, sizeof(Game_Input), &bytes_written, 0); 
        int err = GetLastError();
        OutputDebugStringA("recording\n");
    }

    CloseHandle(record_playback->snapshot);

    if (record_playback->delete_recording == true)
    {
        DeleteFileA(file_name);
        record_playback->delete_recording = false;
    }
}
*/
int CALLBACK WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR     lpCmdLine,
    int       nCmdShow) 
{
    WNDCLASSEX WndClass = {};
    WndClass.cbSize = sizeof(WNDCLASSEX);
    WndClass.style = CS_VREDRAW;
    WndClass.lpfnWndProc = &WinProc;
    WndClass.hInstance = hInstance;
    WndClass.lpszClassName = "game";
    RegisterClassEx(&WndClass);
    DWORD wStyle = WS_OVERLAPPEDWINDOW | WS_CAPTION;
    HWND hwnd = CreateWindowEx(
        CS_OWNDC,
        "game", 
        "project_engine", 
        wStyle,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        1920,
        1080,
        0,
        0,
        hInstance,
        0
    );

    hdc = GetDC(hwnd);

    ShowWindow(hwnd, SW_SHOWDEFAULT);

    // TODO: Use opengl after doing some software render
    //Opengl opengl = {};
    //LoadOpenGL(hwnd, &opengl);

    RECT win_rect = {};
    GetClientRect(hwnd, &win_rect);

    s32 game_alloc_size = 1024 * 1024 * 200;
    s32 temp_alloc_size = 1024 * 1024 * 200;

    Game_Memory *game_memory = (Game_Memory *)VirtualAlloc(0, game_alloc_size, MEM_COMMIT|MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    game_memory->size = game_alloc_size - sizeof(Game_Memory);
    game_memory->base = (u8 *)game_memory + sizeof(Game_Memory);
    game_memory->current = game_memory->base;

    Platform *platform = PushStruct(game_memory, Platform);

    Scratch_Memory *scratch = &game_memory->scratch;
    scratch->size = temp_alloc_size;
    scratch->base = VirtualAlloc(0, scratch->size, MEM_COMMIT|MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    scratch->current = scratch->base;

    // Bind the functions the game needs from win32 to be called later.
    platform->RenderScreen = Win32RenderScreen;
    platform->OpenFile = Win32OpenFile;
    platform->CloseFile = Win32CloseFile;
    platform->WriteFile = Win32WriteFile;
    platform->ReadEntireFile = Win32ReadEntireFile;
    platform->RemoveFile = Win32RemoveFile;
    platform->GetFileTime = Win32GetFileTime;
    platform->Sleep = Win32Sleep;
    platform->StartCount = Win32StartCount;
    platform->EndCount = Win32EndCount;

  
    // DLL info to reload, this is for debug only build
    // TODO: Separate this into a debug branch condition
    Load_Info_DLL dll_info = {};
    CopyFileA("game.dll", "temp_game.dll", false);
    dll_info.temp_dll = LoadLibraryA("temp_game.dll");
    UpdateGame = (UpdateGame_ *)(GetProcAddress(dll_info.temp_dll, "UpdateGame"));

    Input *input = PushStruct(game_memory, Input);
    input->is_game_running = true;
    input->window_height = (win_rect.bottom - win_rect.top);
    input->window_width = (win_rect.right - win_rect.left);

    global_bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
    global_bmi.bmiHeader.biWidth       = input->window_width;
    global_bmi.bmiHeader.biHeight      = input->window_height;
    global_bmi.bmiHeader.biPlanes      = 1;
    global_bmi.bmiHeader.biCompression = BI_RGB;
    global_bmi.bmiHeader.biBitCount    = 32;

    Performance_Counter performance_counter = {};
    performance_counter.start = {};
    performance_counter.end = {};
    performance_counter.frequency = {};

    RECT rect = {};
    GetWindowRect(hwnd, &rect);
    s32 mouse_center_x = (s32)((rect.right - rect.left) / 2);
    s32 mouse_center_y = (s32)((rect.bottom - rect.top) / 2);

    // Absolute screen coordinate values [0, 65536]
    INPUT mouse_input = {};
    mouse_input.type = INPUT_MOUSE;
    mouse_input.mi.dwFlags = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE;
    SendInput(1, &mouse_input, sizeof(INPUT)); // Send a mouse move event to initialize coordinates.

    MSG msg = {};
    while (input->is_game_running) 
    {
//      TODO: Re-enable this at some points.
//      ReloadGameDll(&dll_info, "game.dll", "temp_game.dll", 500);
        HandleInput(input, hwnd, msg);

//      TODO: Do proper game recording some other time
//      HandleRecording(input, &record_playback, "snapshot.save");
       
        UpdateGame(platform, game_memory, input);
    } 

    VirtualFree(game_memory->base, 0, MEM_RELEASE);
    ReleaseDC(hwnd, hdc);
    return 0;
};

