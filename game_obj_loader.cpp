// TODO: Since we want to do this in the game layer. Let's separate the platform i/o code (windows) into where it belongs.
void LoadObj(Game *game, Obj *obj, char* file_name)
{
    Game_Memory *memory = game->memory;
    Platform *platform = game->platform_api;

    File file = {};
    file.name = file_name;
    platform->OpenFile(&file);

    Scratch_Memory *scratch = BeginGameScratch(memory);
    // File size is known after opening.
    file.data = PushArrayScratch(scratch, char, file.size);
    // Read into file data.
    platform->ReadEntireFile(&file);

    s32 line_count = 0;

    // TODO: Correctly assess how much memory is needed otherwise crashing is likely due to the way the obj parse just keeps iterating
    // over the memory without guard checks.
    // TODO(CLEANUP): Guesstimating amount needed.
    obj->vert_num =    10000;
    obj->vertext_num = 10000;
    obj->normal_num =  10000;
    obj->face_num =    10000;

    obj->vertices = PushArray(memory, v4, obj->vert_num);
    obj->vertexts = PushArray(memory, v4, obj->vertext_num);
    obj->normals = PushArray(memory, v4, obj->normal_num);

    obj->vert_indices = PushArray(memory, v4, obj->face_num);
    obj->vertext_indices = PushArray(memory, v4, obj->face_num);
    obj->normal_indices = PushArray(memory, v4, obj->face_num);

    v4 *vertices = obj->vertices;
    v4 *vertexts = obj->vertexts;
    v4 *normals  = obj->normals;

    v4 *vert_indices    = obj->vert_indices;
    v4 *vertext_indices = obj->vertext_indices;
    v4 *normal_indices  = obj->normal_indices;

    s32 vert_count = 0;
    s32 vertext_count = 0;
    s32 normal_count = 0;
    s32 face_count = 0;

    // TODO(CLEANUP): Pretty messy simple obj loading.
    for (s32 i = 0; i < file.size; ++i)
    {
        if (file.data[i] == 'v')
        {
            if (file.data[i + 1] == ' ')
            {
                i = i + 1;
                while (file.data[i] == ' ') i++;

                char buf[30];
                s32 c = 0;
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertices->x =  (f32)atof(buf);

                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertices->y =  (f32)atof(buf);

                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != '\n')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertices->z =  (f32)atof(buf);
                vertices->w = 1;
                vertices++;
                vert_count++;
            }
            else if (file.data[i + 1] == 't')
            {
                i = i + 2;
                while (file.data[i] == ' ') i++;

                char buf[30];
                s32 c = 0;
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertexts->x =  (f32)atof(buf);

                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != '\n')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertexts->y = (f32)atof(buf);
                vertexts->w =  1;
                vertexts++;
                vertext_count++;
            }
            else if (file.data[i + 1] == 'n')
            {
                i = i + 2;
                while (file.data[i] == ' ') i++;

                char buf[30];
                s32 c = 0;
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                normals->x =  (f32)atof(buf);

                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                normals->y = (f32)atof(buf);

                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != '\n')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                normals->z =  (f32)atof(buf);
                normals->w =  1;
                normals++;
                normal_count++;
            }
        }
        else if (file.data[i] == 'f')
        {
            if (file.data[i + 1] == ' ')
            {
                i = i + 1;
                while (file.data[i] == ' ') i++;

                char buf[30];
                memset(buf, 0, 30);
                s32 c = 0;
                while (file.data[i] != ' ' && file.data[i] != '/')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vert_indices->x = (f32)atoi(buf);

                memset(buf, 0, 30);
                c = 0;
                i++;
                while (file.data[i] != ' ' && file.data[i] != '/')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertext_indices->x = (f32)atoi(buf);

                memset(buf, 0, 30);
                c = 0;
                i++;
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                normal_indices->x = (f32)atoi(buf);

                //-----------------------------------
                while (file.data[i] != ' ') i++;

                //-----------------------------------
                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != ' ' && file.data[i] != '/')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vert_indices->y = (f32)atoi(buf);

                memset(buf, 0, 30);
                c = 0;
                i++;
                while (file.data[i] != ' ' && file.data[i] != '/')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertext_indices->y = (f32)atoi(buf);

                memset(buf, 0, 30);
                c = 0;
                i++;
                while (file.data[i] != ' ')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                normal_indices->y = (f32)atoi(buf);

                while (file.data[i] != ' ') i++;

                while (file.data[i] == ' ') i++;
                c = 0;
                memset(buf, 0, 30);
                while (file.data[i] != '/')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vert_indices->z = (f32)atoi(buf);

                memset(buf, 0, 30);
                c = 0;
                i++;
                while (file.data[i] != '/')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                vertext_indices->z = (f32)atoi(buf);

                memset(buf, 0, 30);
                c = 0;
                i++;
                while (file.data[i] != '\n')
                {
                    buf[c] = file.data[i];
                    i++;
                    c++;
                }
                normal_indices->z = (f32)atoi(buf);
                vert_indices->w = 1;
                vertext_indices->w = 1;

                vert_indices++;
                vertext_indices++;
                normal_indices++;

                while (file.data[i] != '\n') i++;
                face_count++;
                //if (face_count == obj->face_num) break;
            }
        }
    }

    obj->vert_num = vert_count;
    obj->vertext_num = vertext_count;
    obj->normal_num = normal_count;
    obj->face_num = face_count;
    
    platform->CloseFile(&file);

    EndScratch(scratch);
}

// TODO: Implement FreeObj
void FreeObj(Obj *obj)
{
    /*
    free(obj->vertices);
    free(obj->vertexts);
    free(obj->normals);

    free(obj->vert_indices);
    free(obj->vertext_indices);
    free(obj->normal_indices);
    */
}
