static void InitGameUI(Game *game) {
    Game_State *game_state = game->game_state;
    Game_UI    *game_ui    = game_state->game_ui;
    Renderer   *renderer   = game->renderer;
    Font       *font       = &game->font;

    s32 button_max_count = 100;
    game_ui->buttons     = PushArray(game->memory, Button, button_max_count);
    game_ui->screen      = renderer->screen;
    game_ui->font        = font;
    game_ui->input       = game->input;
    game_ui->selection   = game_state->selection;

    Button *buttons = game_ui->buttons;
    Button_Theme default_theme = {};
    for (s32 index = 0; index < button_max_count; ++index)
    {
        buttons[index].theme = default_theme;
    }
}

static void EndFrameUI(Game_UI *game_ui) {
    game_ui->current_button_index = 0;
}

// @CHECK: Should this really exist?
// X, Y start at top left.
// The maximum absolute value of radius is half the panel height
static inline void DoPanel(Rect *screen, f32 offset_x, f32 offset_y, s32 width, s32 height, s32 radius, s32 color) {
    s32 sh = screen->height - 1; // Because height range [0, height - 1].

    // Counter clockwise from top left.
    Shape_Rect o0;
    Shape_Rect o1;
    Shape_Rect o2;
    Shape_Rect o3;

    o0.x0 = offset_x;
    o0.y0 = sh - offset_y - radius;
    o0.x1 = offset_x + radius;
    o0.y1 = sh - offset_y;

    o1.x0 = offset_x;
    o1.y0 = sh - offset_y - height;
    o1.x1 = offset_x + radius;
    o1.y1 = sh - offset_y - height + radius;

    o2.x0 = offset_x + width - radius;
    o2.y0 = sh - offset_y - height;
    o2.x1 = offset_x + width;
    o2.y1 = sh - offset_y - height + radius;

    o3.x0 = offset_x + width - radius;
    o3.y0 = sh - offset_y - radius;
    o3.x1 = offset_x + width;
    o3.y1 = sh - offset_y;

    // Counter clock wise top first center last.
    Shape_Rect i0;
    Shape_Rect i1;
    Shape_Rect i2;
    Shape_Rect i3;
    Shape_Rect i4;

    i0.x0 = o0.x1;
    i0.y0 = o0.y0;
    i0.x1 = o3.x0;
    i0.y1 = o3.y1;

    i1.x0 = o1.x0;
    i1.y0 = o1.y1;
    i1.x1 = o0.x1;
    i1.y1 = o0.y0;

    i2.x0 = o1.x1;
    i2.y0 = o1.y0;
    i2.x1 = o2.x0;
    i2.y1 = o2.y1;

    i3.x0 = o2.x0;
    i3.y0 = o2.y1;
    i3.x1 = o3.x1;
    i3.y1 = o3.y0;

    i4.x0 = o1.x1;
    i4.y0 = o1.y1;
    i4.x1 = o3.x0;
    i4.y1 = o3.y0;

    DrawQuad(screen, &i0, color);
    DrawQuad(screen, &i1, color);
    DrawQuad(screen, &i2, color);
    DrawQuad(screen, &i3, color);
    DrawQuad(screen, &i4, color);

    s32 subarc_pixels   = 5;
    s32 arc_length      = (s32)(TAU*radius * 0.25f);
    s32 sections        = arc_length / subarc_pixels;
    f32 dtheta          = 90.0f / sections;
    f32 theta           = dtheta;

    v4 l2 = {};
    v4 l1 = {};

    s32 x_dir = 0;
    s32 y_dir = 0;

    f32 radian = TAU/360.0f;
    
    for (s32 c = 0; c < 4; ++c) {
        if (c == 0) {
            l2 = {o0.x1, o0.y0};
            l1 = {o0.x0, o0.y0};

            x_dir = -1;
            y_dir =  1;
        }

        if (c == 1) {
            l2 = {o1.x1, o1.y1};
            l1 = {o1.x0, o1.y1};

            x_dir = -1;
            y_dir = -1;
        }

        if (c == 2) {
            l2 = {o2.x0, o2.y1};
            l1 = {o2.x1, o2.y1};

            x_dir =  1;
            y_dir = -1;
        }

        if (c == 3) {
            l2 = {o3.x0, o3.y0};
            l1 = {o3.x1, o3.y0};

            x_dir = 1;
            y_dir = 1;
        }

        for (s32 s = 0; s < sections; ++s) {
            f32 x = (f32)cos(theta*radian) * radius * x_dir;
            f32 y = (f32)sin(theta*radian) * radius * y_dir;

            v4 l0 = l1;
            l1 = {l2.x + x, l2.y + y};

            Shape_Rect r;
            r.x0 = (f32)GetMin((s32)floor(l0.x), (s32)floor(l1.x), (s32)floor(l2.x));
            r.y0 = (f32)GetMin((s32)floor(l0.y), (s32)floor(l1.y), (s32)floor(l2.y));

            r.x1 = (f32)GetMax((s32)ceil(l0.x), (s32)ceil(l1.x), (s32)ceil(l2.x));
            r.y1 = (f32)GetMax((s32)ceil(l0.y), (s32)ceil(l1.y), (s32)ceil(l2.y));


            if (c == 0) DrawTriangle(screen, &r, l1, l0, l2, color);
            if (c == 1) DrawTriangle(screen, &r, l2, l0, l1, color);
            if (c == 2) DrawTriangle(screen, &r, l1, l0, l2, color);
            if (c == 3) DrawTriangle(screen, &r, l0, l1, l2, color);

            theta += dtheta;

        }

        theta = dtheta;
    }
}

// Should font be global? So as to remove a parameter from each of the PrintLine family functions.
static inline void PrintLine(Rect *screen, Font *font, char *text, f32 xpos, f32 ypos) {
    Rect *bitmap = &font->bitmap;

    s32 pw = bitmap->width;
    s32 ph = bitmap->height;

    s32 advance_y = 0;

    ypos = (f32)screen->height - ypos;
    for (s32 i = 0; i < strlen(text); ++i) {
        s32 char_index = text[i] - font->first_char;

        stbtt_aligned_quad q = {};
        stbtt_GetPackedQuad(font->chardata, pw, ph, char_index, &xpos, &ypos, &q, 0);

        DrawCharQuad(screen, bitmap, q, advance_y, (s32)ypos);
    }
}

// This print lines starting from the top left of the window. 
// Each print goes down a line.
static inline void PrintLine(Rect *screen, Font *font, char *text) {
    Rect *bitmap = &font->bitmap;

    s32 pw = bitmap->width;
    s32 ph = bitmap->height;
    s32 advance_y = 0;

    f32 xpos = 0;
    f32 ypos = (f32)screen->height - font->line_height;
    for (s32 i = 0; i < strlen(text); ++i)
    {
        s32 char_index = text[i] - font->first_char;

        stbtt_aligned_quad q = {};
        stbtt_GetPackedQuad(font->chardata, pw, ph, char_index, &xpos, &ypos, &q, 0);

        DrawCharQuad(screen, bitmap, q, advance_y, (s32)ypos);
    }
}

// @INPROGRESS: Animation is currently frame dependant. Mouse press and release aren't handled
// well when releasing outside of button area then moving back.
static bool DoButton(Game_UI *game_ui, char *text, s32 color, s32 offset_x, s32 offset_y, s32 width, s32 height) {
    if (game_ui->buttons[game_ui->button_count].id == 0) {
        game_ui->button_count++;
        game_ui->buttons[game_ui->button_count].id = game_ui->button_count;
    }

    bool r = false;
    
    Button *button = &game_ui->buttons[game_ui->current_button_index++];
    s32 *frame_count = &button->frame_count;

    // frame_count starts counting since click and increases every with frame for an amount of frames
    // i.e. 10 frames. Then, it resets.
    Input *input = game_ui->input;
    if (input->lm_state == input->LM_UP && 
        input->mouse_x >= offset_x && input->mouse_x < width + offset_x &&
        input->mouse_y >= offset_y && input->mouse_y < offset_y + height &&
        *frame_count >= 0) {
        r = true;
        input->lm_state = input->LM_DEFAULT;
        *frame_count = 1;
        color = (s32)(color * 0.8f);
    } else if (*frame_count >= 1) {
        color = (s32)(color * 0.8f);
        (*frame_count)++;
    }
    
    if (*frame_count > 3) {
        *frame_count = 0;
    }

    Rect *screen = game_ui->screen;
    Font *font     = game_ui->font;
    DoPanel(screen, (f32)offset_x, (f32)offset_y, width, height, 12, color); // @CHECK: Why is this here?

    s32 line_offset_x = offset_x;
    s32 line_offset_y = offset_y + (s32)(game_ui->font->upper_case_visible_height/2.0f) + height/2;

    PrintLine(screen, font, text, (f32)line_offset_x, (f32)line_offset_y);

    return r;
}

static bool
DoButtonDefault(Game_UI *game_ui, Button_Theme theme, char *text, s32 offset_x, s32 offset_y, s32 width, s32 height) {
    bool r = false;
    if (DoButton(game_ui, text, theme.color, offset_x, offset_y, width, height)) {
        r = true;
    }

    return r;
}

static void
DoEditPanel(Game_UI *game_ui, Button_Theme theme) {
    Rect *screen = game_ui->screen;
    Font *font = game_ui->font;

    m4x4 transform = M4x4();
    if (DoButtonDefault(game_ui, theme, "Go left", 0, 100, 200, 50)) {
        transform = M4x4Translate(-1, 0, 0);
    }

    if (DoButtonDefault(game_ui, theme, "Go right", 0, 152, 200, 50)) {

        transform = M4x4Translate(1, 0, 0);
    }

    if (DoButtonDefault(game_ui, theme, "Go down", 0, 204, 200, 50)) {
        transform = M4x4Translate(0, -1, 0);
    }

    if (DoButtonDefault(game_ui, theme, "Go up", 0, 256, 200, 50)) {
        transform = M4x4Translate(0, 1, 0);
    }

    s32 height = 50;
    for (s32 i = 0; i < game_ui->selection->count; ++i) {
        game_ui->selection->meshes[i]->transform = transform * game_ui->selection->meshes[i]->transform;

        switch (game_ui->selection->meshes[i]->obj_type) {
            case (CHARACTER): {
                PrintLine(screen, font, "Character Selection", 500, (f32)height);
                height += 50;
            } break;

            case (FLOOR): {
                PrintLine(screen, font, "Floor Selection", 500, (f32)height);
                height += 50;
            } break;

            case (CUBE): {
                PrintLine(screen, font, "Cube Selection", 500, (f32)height);
                height += 50;
            } break;
        }
    }
}
