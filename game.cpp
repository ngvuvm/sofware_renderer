/* 
 * @REMINDER (For not obvious, incomplete systems that span across multiple places):
 * @BOTTLENECK: Make a tilemap for various reasons like gameplay editor or actual gameplays.
 * Key input handling is pretty awful at the moment (maybe a cleanup with keystate memory mappings?).
 * Non-existing gameplay editor.
 * Explore more gameplay ideas. 
 * 
*/

#include "game.h"
#include "game_obj_loader.cpp"
#include "game_world.cpp"
#include "game_render.cpp"
#include "game_variables.cpp"
#include "game_ui.cpp"

/* @UPDATE: Finish this sometime.
static void DebugOutputTimer(Debug_Info *debug_info)
{
    Game_State *game_state = &game->game_state;
    debug_info->frames_micro += game_state->t_micro;

    // Since a frame can have very small elapsed time, we normally deal with microseconds before having enough time to 
    // convert back to seconds.
    if (debug_info->frames_micro >= 1000000)
    {
        debug_info->total_sec += debug_info->frames_micro * 0.000001f;

        char msframe[256];
        sprintf_s(msframe, sizeof(msframe), "Game Timer: %fs\n", debug_info->total_sec);

        PrintLine(&game->renderer.screen, &game->font, msframe);

        debug_info->frames_micro = 0; // Reset frames timer.
    }
}
*/

static bool IsKeyDown(Input *input, char c) {
    return (input->key_char == c && input->key_state == input->KEY_DOWN);
}

static bool IsKeyUp(Input *input, char c) {
    return (input->key_char == c && input->key_state == input->KEY_UP);
}

static void UpdatePosition(Game_State *game_state) {
    if (game_state->target_ms <= game_state->t_micro) {
    }

    if (game_state->degrees >= 360.0f) {
        game_state->degrees = 0.0f;
    }
}

// Every call can restart the count.
static void SelectMeshes(Game *game, Renderer *renderer, Mesh *meshes, Shape_Rect mouse_box) {
    Game_State *game_state       = game->game_state;
    Selection  *selection        = game_state->selection;
    Mesh       **selected_meshes = selection->meshes;

    // Check box circle intersect.
    // Quick elimination for aabb case.
    // Handling ellipse major axis as x axis case only because we rotate around x & z. This can change.
    bool restarted = false;
    for (s32 i = 0; i < renderer->mesh_count; ++i) {
        if (meshes[i].obj_type != TARGET_CIRCLE && meshes[i].obj_type != FLOOR) {
            Shape_Rect ellipse_box = meshes[i].circle->bounding_box;
            v2 ellipse_origin = {ellipse_box.x1 - (ellipse_box.x1 - ellipse_box.x0) * 0.5f, ellipse_box.y1 - (ellipse_box.y1 - ellipse_box.y0) * 0.5f};

            v2 p0 = {0.0f, 0.0f}; // closest point.

            if (ellipse_box.x0 <= mouse_box.x1 && ellipse_box.x1 >= mouse_box.x0 && 
                    ellipse_box.y0 <= mouse_box.y1 && ellipse_box.y1 >= mouse_box.y0) {
                if (ellipse_origin.x <= mouse_box.x0) {
                    p0.x = mouse_box.x0;
                } else if (ellipse_origin.x >= mouse_box.x1) {
                    p0.x = mouse_box.x1;
                } else {
                    p0.x = ellipse_origin.x;
                }

                if (ellipse_origin.y <= mouse_box.y0) {
                    p0.y = mouse_box.y0;
                } else if (ellipse_origin.y >= mouse_box.y1) {
                    p0.y = mouse_box.y1;
                } else {
                    p0.y = ellipse_origin.y;
                }

                v2 p1 = {(f32)p0.x, (f32)ellipse_origin.y}; // point on ellipse x axis, this vertex is at the orthogonal angle.

                v2 p2 = {(f32)ellipse_origin.x, (f32)ellipse_origin.y}; // ellipse origin.

                f32 d0 = (f32)sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y)); // y axis
                f32 d1 = (f32)sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y)); // x axus
                f32 d2 = (f32)sqrt(d0 * d0 + d1 * d1); // hypotenuse, also distance from origin to closest point.

                f32 cosine = d1/d2;
                f32 sine   = d0/d2;

                f32 a = ellipse_box.x1 - ellipse_origin.x;
                f32 b = ellipse_box.y1 - ellipse_origin.y;
                s32 d = (s32)sqrt(a*a*b*b/(b*b*cosine*cosine + a*a*sine*sine));

                // Check collision.
                if ((d2 - d <= 0) || (p0.x == p1.x && p0.y == p1.y)) {
                    // If at least one is found, restart the count. This means selection keeps old selected meshes if nothing is found.
                    if (!restarted) {
                        selection->count = 0;
                        restarted = true;
                    }

                    selected_meshes[selection->count] = &meshes[i];
                    selection->count++;
                }
            }
        }
    }
}

static void InitGameState(Game *game, Tile_Map *tile_map) {
    Game_Memory *game_memory = game->memory;
    game->game_state = PushStruct(game_memory, Game_State);
    Game_State *game_state = game->game_state;

    game_state->tile_map = tile_map;
    game_state->target_ms = (1.0f / 60.0f) * 1000.0f;
    game_state->mode = EDIT_MODE;
    game_state->selection = PushStruct(game_memory, Selection);
    game_state->selection->meshes = PushArray(game_memory, Mesh *, game->renderer->mesh_count);
    game_state->game_ui = PushStruct(game_memory, Game_UI);
}
//
// GLOBALS
global Vars_Info vars_list;
global File vars_file;
Game *game;

extern "C" __declspec(dllexport) UPDATEGAME(UpdateGame) {
    Performance_Counter performance_counter = {};
    platform->StartCount(&performance_counter);
    if (!input->is_game_initialized) {
        // Initialize game structure.
        game = PushStruct(game_memory, Game);

        game->memory = game_memory;
        game->platform_api = platform;
        game->input = input;
        game->renderer = PushStruct(game_memory, Renderer);

        InitRenderer(game, input->window_width, input->window_height);

        Renderer *renderer    = game->renderer;
        Mesh *meshes          = renderer->meshes;
        Camera *camera        = &renderer->camera;
        Texture *wall_texture = &renderer->textures[0];

        // Init globals.
        vars_list = InitVarsList(game);
        
        PushRenderObject(game, CHARACTER); 
        PushRenderObject(game, FLOOR); 
        PushRenderObject(game, CUBE); 
        PushRenderObject(game, TARGET_CIRCLE); 

        Tile_Map *tile_map = PushStruct(game_memory, Tile_Map);
        // @UPDATE: Move this out eventually.
        {
            tile_map->tile_width  = 2;
            tile_map->tile_height = 2;
            tile_map->row_count   = 4;
            tile_map->col_count   = 4;
            tile_map->tile_count  = tile_map->row_count * tile_map->col_count;
            tile_map->tiles       = PushArray(game_memory, Tile, tile_map->tile_count); 

            for (s32 row_index = 0; row_index < tile_map->row_count; ++row_index) {
                for (s32 col_index = 0; col_index < tile_map->col_count; ++col_index) {
                    s32 tile_index = tile_map->col_count * row_index + col_index;
                    tile_map->tiles[tile_index].position = {(f32)col_index, (f32)row_index};
                    tile_map->tiles[tile_index].to_visual_position = M4x4();
                }
            } 
        }

        PushRenderMesh(renderer, tile_map, {1, 1, 0}, M4x4Translate(0, 0, 0.45) * M4x4RotateX(90) * M4x4Scale(0.5, 0.5, 0.5), DIFFUSE, CHARACTER, NULL, 0x00FFFFFF);
        PushRenderMesh(renderer, tile_map, {2, 1, 0}, M4x4Translate(0, 0, 0.45) * M4x4RotateX(90) * M4x4Scale(0.5, 0.5, 0.5), DIFFUSE, CHARACTER, NULL, 0x00FFFFFF);
        PushRenderMesh(renderer, tile_map, {1, 2, 0}, M4x4Translate(0, 0, 0.6)  * M4x4Scale(0.5, 0.5, 0.5), DIFFUSE, CUBE, NULL, 0x00FFFFFF);
        PushRenderMesh(renderer, tile_map, {2, 2, 0}, M4x4Translate(0, 0, 0.6)  * M4x4Scale(0.5, 0.5, 0.5), DIFFUSE, CUBE, NULL, 0x00FFFFFF);

        // Draw circles for each non floor mesh.
        for (s32 mesh_index = 0; mesh_index < renderer->mesh_count; ++mesh_index) {
            if (meshes[mesh_index].obj_type != FLOOR && meshes[mesh_index].obj_type != TARGET_CIRCLE) { 
                PushRenderMesh(renderer, tile_map, {0, 0, 0}, M4x4Translate(0, 0, 0.15), DIFFUSE, TARGET_CIRCLE, NULL, 0x7300FF00);
                meshes[mesh_index].circle = &meshes[renderer->mesh_count-1];
            }
        }

        stbi_set_flip_vertically_on_load(true);
        wall_texture->data = stbi_load("data/images/wall.jpg", &wall_texture->width, &wall_texture->height, &wall_texture->n_channel, 4);

        for (s32 i = 0; i < tile_map->row_count; ++i) {
            for (s32 j = 0; j < tile_map->col_count; ++j) {
                PushRenderMesh(renderer, tile_map, {(f32)j, (f32)i}, M4x4(), DIFFUSE, FLOOR, wall_texture, 0x00FFFFFF);

            }
        }

        // TODO: Different strategy for file space memory that will get mapped and unmapped from the program every modification.
        // NOTES: See game_variables.h
        //WriteVars(game);

        vars_file.name = "game.variables";

        // ReadVars messes up the target_struct memory address.
        /*
        if (CheckVarsReload(game, &vars_file, &vars_list))
        {
            ReadVars(game, &vars_file, &vars_list, &camera, "Camera");
        }
        */

        // Initialize font.
        Font *font = &game->font;
        font->first_char = 32;
        font->num_chars = 96;
        font->line_height = 20.0f;
        font->chardata = PushArray(game->memory, stbtt_packedchar, 96);

        Rect *bitmap = &font->bitmap;
        bitmap->width = 512;
        bitmap->height = 512;

        bitmap->data = PushArray(game->memory, unsigned char, bitmap->width * bitmap->height);

        File file = {};
        file.name = "C:/windows/fonts/arial.TTF";

        platform->OpenFile(&file);
        file.data = (char *)PushArray(game->memory, char, file.size);
        platform->ReadEntireFile(&file);
        platform->CloseFile(&file);

        stbtt_GetScaledFontVMetrics((unsigned char *)file.data, 0, font->line_height, &font->ascent, &font->descent, &font->line_gap);

        stbtt_fontinfo info;
        stbtt_InitFont(&info, (unsigned char *)file.data, 0);
        s32 x0, y0, x1, y1;

        // @CHECK: Not liking this. Since each glyph can maybe have different height, it maybe better to
        // check all the glyphs in the text first to calculate the height. But then it might be slow.
        // Sticking with this for now but it's gross.
        {
            // Picking a random upper case character height
            stbtt_GetCodepointBox(&info, 'O', &x0, &y0, &x1, &y1);
            f32 scale = stbtt_ScaleForPixelHeight(&info, font->line_height);

            x0 = (s32)(x0 * scale);
            y0 = (s32)(y0 * scale);
            x1 = (s32)(x1 * scale);
            y1 = (s32)(y1 * scale);

            font->upper_case_visible_height = y1 - y0;

            // Picking a random lower case character height
            stbtt_GetCodepointBox(&info, 'a', &x0, &y0, &x1, &y1);

            x0 = (s32)(x0 * scale);
            y0 = (s32)(y0 * scale);
            x1 = (s32)(x1 * scale);
            y1 = (s32)(y1 * scale);

            font->lower_case_visible_height = y1 - y0;
        }

        stbtt_pack_context context;

        s32 result = stbtt_PackBegin(&context, bitmap->data, bitmap->width, bitmap->height, 0, 1, PushArray(game->memory, char, sizeof(stbtt_pack_context)));
        if (result) {
            s32 h_oversample = 2;
            s32 v_oversample = 2;

            stbtt_PackSetOversampling(&context, h_oversample, v_oversample);
            result = stbtt_PackFontRange(&context, (u8*)file.data, 0, font->line_height, 32, 95, font->chardata);

            stbtt_PackEnd(&context);
        } else {
            // TODO: Log error. Just crash for now.
            Assert(0);
        }

        InitGameState(game, tile_map);
        InitGameUI(game);

        input->is_game_initialized = true;
    }

    Renderer   *renderer   = game->renderer;
    Game_State *game_state = game->game_state;
    Game_UI    *game_ui    = game_state->game_ui;
    f32        *zbuffer    = renderer->zbuffer;
    Rect       *screen     = renderer->screen;
    Mesh       *meshes     = renderer->meshes;
    Camera     *camera     = &renderer->camera;

    // ALL MODE INPUTS
    if (IsKeyUp(input, 'O')) {
        game_state->mode = (game_state->mode == PLAY_MODE ? EDIT_MODE : PLAY_MODE);
        input->key_state = {};
    }

    if (IsKeyDown(input, 'Q')) {
        input->is_game_running = false;
        // Remove comment if this is necessary
        // stbi_image_free(renderer->textures[0].data);
        return;
    }

    game_state->t_micro_accumulator += game_state->t_micro;
    f32 target_micro = game_state->target_ms * 1000.f;
    static Shape_Rect mouse_box = {};
    static bool mouse_box_on = false;

    while (game_state->t_micro_accumulator >= target_micro) {
        game_state->t_micro_accumulator -= target_micro;

        f32 velocity = 4.f; // per second
        f32 dt = game_state->target_ms * 0.001f; // in seconds
        f32 pos = dt * velocity;

        static s32 occupied_tile_position = -1;
        switch (game_state->mode) {
            case PLAY_MODE: {
                m4x4 transform = M4x4();
                if (IsKeyDown(input, 'W')) {
                    pos *= 1;
                    transform = M4x4Translate(0, pos, 0);
                }

                if (IsKeyDown(input, 'A')) {
                    pos *= -1;
                    transform = M4x4Translate(pos, 0, 0);
                }

                if (IsKeyDown(input, 'S')) {
                    pos *= -1;
                    transform = M4x4Translate(0, pos, 0);
                }

                if (IsKeyDown(input, 'D')) {
                    pos *= 1;
                    transform = M4x4Translate(pos, 0, 0);
                }

                if (input->lm_state == input->LM_DOWN) {
                    s32 real_mouse_y      = input->window_height - input->mouse_y;
                    s32 real_last_mouse_y = input->window_height - input->last_mouse_y;

                    s32 bot_left_x  = input->last_mouse_x < input->mouse_x ? input->last_mouse_x : input->mouse_x;
                    s32 bot_left_y  = real_last_mouse_y < real_mouse_y ? real_last_mouse_y : real_mouse_y;
                    s32 top_right_x = input->last_mouse_x > input->mouse_x ? input->last_mouse_x : input->mouse_x;
                    s32 top_right_y = real_last_mouse_y > real_mouse_y ? real_last_mouse_y : real_mouse_y;

                    mouse_box = {(f32)bot_left_x, (f32)bot_left_y, (f32)top_right_x, (f32)top_right_y};
                    mouse_box_on = true;
                }

                if (input->lm_state == input->LM_UP) {
                    input->lm_state = input->LM_DEFAULT;
                    mouse_box_on = false;
                    SelectMeshes(game, renderer, meshes, mouse_box);
                }

                Mesh **selected_meshes = game_state->selection->meshes;
                for (s32 i = 0; i < game_state->selection->count; ++i) {
                    selected_meshes[i]->transform = transform * selected_meshes[i]->transform;
                }
            } break;

            // TAKE INPUTS IN EDIT MODE.
            case EDIT_MODE: {
                /*
                // Camera tilt aka camera->lookat_tilt needs a rework.
                if (input->left_click_down)
                {
                    f32 mouse_delta_x = (f32)(input->mouse_y - input->last_mouse_y);
                    f32 mouse_delta_y = (f32)(input->mouse_x - input->last_mouse_x);
                    camera->lookat_tilt = M4x4RotateX(mouse_delta_x/10.0f) * M4x4RotateY(mouse_delta_y/10.0f);

                    if (input->last_mouse_x != input->mouse_x || input->last_mouse_y != input->mouse_y)
                    {
                        camera->lookat = camera->lookat_tilt * camera->lookat;
                    }

                    input->last_mouse_x = input->mouse_x;
                    input->last_mouse_y = input->mouse_y;
                }
                */

                if (input->rm_state == input->RM_DOWN) {
                    f32 mouse_multiplier = 0.3;

                    if (input->last_mouse_x != input->mouse_x || input->last_mouse_y != input->mouse_y) {

                        camera->rotate_x += (input->mouse_y - input->last_mouse_y) * mouse_multiplier;

                        // Determine the direction of x axis rotation when y axis changes.
                        if (input->first_click) {
                            if (camera->rotate_x > 0 && camera->rotate_x <=  180 ||
                                camera->rotate_x < -90 && camera->rotate_x >= -270) {
                                camera->zdir = -1;
                            }
                            else {
                                camera->zdir = 1;
                            }
                            input->first_click = false;
                        }

                        camera->rotate_z += (input->mouse_x - input->last_mouse_x) * mouse_multiplier * camera->zdir;

                        camera->rotate = M4x4RotateX(camera->rotate_x) * M4x4RotateY(camera->rotate_y) * M4x4RotateZ(camera->rotate_z);
                    }

                    if (camera->rotate_x >= 360.0f || camera->rotate_x <= -360.0f)
                        camera->rotate_x = (f32)((s32)camera->rotate_x % 360);
                    if (camera->rotate_z >= 360.0f || camera->rotate_z <= -360.0f)
                        camera->rotate_z = (f32)((s32)camera->rotate_z % 360);

                    input->last_mouse_x = input->mouse_x;
                    input->last_mouse_y = input->mouse_y;
                }

                if (input->rm_state == input->RM_UP) {
                    input->first_click = true;
                    input->rm_state = input->RM_DEFAULT;
                }

                // Camera zoom.
                if (input->is_scrolling) {
                    f32 scroll_multiplier = 5.0f;
                    f32 scroll_amount = input->scroll_amount >= 0 ? pos : -pos;
                    scroll_amount *= scroll_multiplier;

                    camera->translate = camera->translate * M4x4Translate(0, 0, scroll_amount);
                    input->is_scrolling = false;
                }

                // Camera panning.
                if (IsKeyDown(input, 'W')) {
                    camera->translate = camera->translate * M4x4Translate(0, -pos, 0);
                }

                if (IsKeyDown(input, 'A')) {
                    camera->translate = camera->translate * M4x4Translate(pos, 0, 0);
                }

                if (IsKeyDown(input, 'S')) {
                    camera->translate = camera->translate * M4x4Translate(0, pos, 0);
                }

                if (IsKeyDown(input, 'D')) {
                    camera->translate = camera->translate * M4x4Translate(-pos, 0, 0);
                }

            } break;
        }

        // Update the camera transform after previous modifications.
        camera->lookat = camera->lookat_tilt * camera->lookat_rotate * camera->lookat_translate * camera->translate * camera->rotate;
    }

    // Update world positions
    {
        for (s32 i = 0; i < renderer->mesh_count; ++i) {
            if (meshes[i].obj_type != FLOOR && meshes[i].obj_type != TARGET_CIRCLE) {
                meshes[i].world_position = ExtractMatrixColumn(meshes[i].transform, 3);
                meshes[i].circle->transform.m[3]  = meshes[i].world_position.x;
                meshes[i].circle->transform.m[7]  = meshes[i].world_position.y;

            }
        }
    }

    // Right now this is always true.
    if (renderer->do_depth) {
        __m256 zval = _mm256_set1_ps(0.0f);
        s32 left_over_bytes = screen->size % 8;
        f32 *zdata = zbuffer;

        for (s32 i = 0; i < left_over_bytes; ++i) {
            *zdata = 0.0f;
            zdata = (f32 *)((u8 *)zdata + 1);
        }

        for (s32 i = 0; i < screen->area - left_over_bytes; i += 8, zdata += 8) {
            _mm256_storeu_ps(zdata, zval);
        }

        // @REPLACE: This should be set like above as well instead of C's memset.
        memset(renderer->shadow_zbuffer, 0, input->window_width * input->window_height * 4);

        ClearScreen(screen, 0x00001F3F);

        // Create camera at the light position to render scence once to fill the shadow map depth buffer.
        // The actual zbuffer for the scene will be filled later in the next DrawMeshes call.
        // Then the coordinates at the pixel in the draw loop would be transformed to be looked
        // from the light camera position. Then, its' transformed z value is compared to the 
        // shadow z buffer value of the one at the same pixel location. After that, we know if it's in 
        // the shadow or not.
        
        // @UPDATE: light_dir is some fixed v4 right now instead of a more robust thing.
        m4x4 rotate_to_light = M4x4RotateZ(90) * M4x4RotateX(30);
        if (renderer->options == SHADOW_OFF) {
            camera->light_dir = rotate_to_light * Normalize(v4{0, 0, 10, 1});
        } else if (renderer->options == SHADOW_ON) { // Update the camera with current frame's camera transforms.
            Camera light_cam;

            // Setup a camera at light's position.
            InitCamera(&light_cam, ORTHOGRAPHIC, 10.0f, renderer->screen->width, renderer->screen->height, rotate_to_light);

            // Set renderer to render to shadow buffer with light camera position.
            camera->light_dir = light_cam.light_dir;
            DrawMeshes(renderer, &light_cam, renderer->shadow_zbuffer, true);

            renderer->shadow_transform = light_cam.projection * light_cam.lookat * M4x4Inverse(camera->lookat) * M4x4Inverse(camera->projection);
        }
    }

    // ReadVars messes up the target_struct memory address.
    /*
    if (CheckVarsReload(game, &vars_file, &vars_list))
    {
        ReadVars(game, &vars_file, &vars_list, camera, "Camera");
    }
    */

    DrawMeshes(renderer, camera, renderer->zbuffer, false);

    // UI
    {
        Font *font = &game->font;

        // OPTIONS.
        char debug_info[256];

        // Shadow.
        if (renderer->options == SHADOW_ON) {
            sprintf_s(debug_info, "SHADOW_ON");
        } else if (renderer->options == SHADOW_OFF) {
            sprintf_s(debug_info, "SHADOW_OFF");
        }
        PrintLine(screen, font, debug_info);

        // Game mode.
        if (game_state->mode == PLAY_MODE) {
            sprintf_s(debug_info, "PLAY_MODE");
        } else if (game_state->mode == EDIT_MODE) {
            sprintf_s(debug_info, "EDIT_MODE");
        }

        PrintLine(screen, font, debug_info, 0, 40);

        // Frame time.
        sprintf_s(debug_info, "Frame time: %f ms", game_state->t_micro / 1000.0f);
        PrintLine(screen, font, debug_info, 0, 80);

        s32 color = 0x000000FF;
        sprintf_s(debug_info, "TOGGLE SHADOW");
        if (DoButton(game_ui, debug_info, color, input->window_width - 300, 0, 300, 50)) {
            renderer->options = (renderer->options == SHADOW_OFF ? SHADOW_ON : SHADOW_OFF);
        }

        
        // Editor panel and mesh selection logic.
        {
            Button_Theme default_theme = {};
            DoEditPanel(game_ui, default_theme);
        }
    }

    // Draw mouse selection
    if (mouse_box_on) {
        DrawQuad(screen, &mouse_box, 0xAA00DD00);
    }

    platform->RenderScreen(input->window_width, input->window_height, screen->width, screen->height, screen->data);

    game_state->is_video_sync = true;
    if (game_state->is_video_sync == true) {
        // TODO: Implement an accurate sleep timer.
        s32 sleep_ms = (s32)((game_state->target_ms * 1000.0f - game_state->t_micro) / 1000.0f);
        char msframe[256];

#if DEBUG
        sprintf_s(msframe, sizeof(msframe), "elapsed: %f ms\n", game_state->t_micro / 1000.0f);
        sprintf_s(msframe, sizeof(msframe), "sleep: %d ms\n", sleep_ms);
#endif

//        if (sleep_ms > 0) platform->Sleep(sleep_ms);
    }

    EndFrameUI(game_ui);

    game_state->t_micro =  platform->EndCount(&performance_counter);
}
