static inline void InitRect(Game *game, Rect *rect, s32 x, s32 y, s32 width, s32 height) {
    rect->width  = width;
    rect->height = height;
    rect->area   = width * height;
    rect->size   = rect->area * 4;
    rect->data   = PushArray(game->memory, u8, rect->size);
}

// NOTE: Camera looks down to the ground.
static inline void InitCamera(Camera *camera, Projection_Type projection_type, f32 focal_length, s32 width, s32 height, m4x4 from_transform) {
    camera->fov = 60;
    camera->viewport = M4x4ViewPort(0, 0, width, height);
    if (projection_type == PERSPECTIVE)
    {
        camera->projection = M4x4Perspective(camera->fov, (f32)width/height, 100.0f, 0.1f);
    }
    else
    {
        f32 rad_fov = (camera->fov/2) * (PI/180);
        // z_length is the distance from target object to camera. 
        f32 scale = (f32)tan(rad_fov) * focal_length;

        camera->projection = M4x4Orthographic(scale, (f32)width/height, 100.0f, 0.1f);
    }

    v4 to = {0, 0, 0, 0};
    v4 from = from_transform * v4{0, 0, 3.5, 0};

    v4 world_up = Normalize(M4x4RotateY(90) * v4{0, 1, 0, 0});
    v4 forward = Normalize(from - to);
    v4 right = Normalize(Cross(world_up , forward));
    v4 up = Normalize(Cross(forward, right));

    camera->lookat_rotate =
    {
        right.x,   right.y,   right.z,   0,
        up.x,      up.y,      up.z,      0,
        forward.x, forward.y, forward.z, 0,
        0,         0,         0,         1
    };

    // Not actually rotating/translating on the camera but directly on the vertices.
    camera->rotate = M4x4RotateX(camera->rotate_x) * M4x4RotateY(camera->rotate_y) * M4x4RotateZ(camera->rotate_z);
    camera->translate = M4x4Translate(camera->translate_x, camera->translate_y, camera->translate_z);

    // These lookat m4x4 transform object vertices by inversing the transform to camera 
    // postion (rotate & translate). Because moving to camera is the same as moving the world
    // in a particular way (inverse way).
    camera->lookat_translate = M4x4Translate(-from.x, -from.y, -from.z);
    camera->lookat = camera->lookat_rotate * camera->lookat_translate * camera->translate * camera->rotate;

    camera->light_dir = Normalize(from - to);
    camera->lookat_tilt = M4x4();
}

// TODO: Doing static array on the stack right now, switch to allocated memory.
static inline void InitMesh(Mesh *mesh, Obj *obj, m4x4 transform, Object_Type obj_type, Materials mat_type, Texture *texture, s32 color) {
    mesh->vertices        = obj->vertices;
    mesh->normals         = obj->normals;
    mesh->vertexts        = obj->vertexts;

    mesh->vert_indices    = obj->vert_indices;
    mesh->vertext_indices = obj->vertext_indices;
    mesh->normal_indices  = obj->normal_indices;

    mesh->color           = color;

    mesh->face_num        = obj->face_num;

    mesh->transform       = transform;

    mesh->enable_draw     = true;

    mesh->obj_type        = obj_type;
    mesh->mat_type        = mat_type;
    
    if (texture) mesh->texture = *texture;
}

static inline void InitRenderer(Game *game, s32 width, s32 height) {
    // Static number of meshes for the renderer to loop through.
    s32 mesh_count = 1000;
    s32 object_count = OBJECT_COUNT;
    s32 texture_count = 10;
    
    Renderer *renderer = game->renderer;
    renderer->screen   = PushStruct(game->memory, Rect);
    InitRect(game, renderer->screen, 0, 0, width, height);

    renderer->zbuffer        = PushArray(game->memory, f32, renderer->screen->area);
    renderer->shadow_zbuffer = PushArray(game->memory, f32, renderer->screen->area);
    renderer->meshes         = PushArray(game->memory, Mesh, mesh_count);
    renderer->objects        = PushArray(game->memory, Obj, object_count);
    renderer->textures       = PushArray(game->memory, Texture, texture_count);
    renderer->do_depth       = true;

    /*
    InitCamera(&renderer->camera, ORTHOGRAPHIC, 1, renderer->screen.width, renderer->screen.height, M4x4());
    */

    InitCamera(&renderer->camera, PERSPECTIVE, 1, renderer->screen->width, renderer->screen->height, M4x4());
}

static inline void InitObj(Game *game, Obj *obj, v4 *vertices, v4 *normals, v4 *vertexts, v4 *vert_indices, v4 *normal_indices, v4 *vertext_indices, s32 vert_num, s32 face_num) {
    Game_Memory *memory = game->memory;
    obj->vertices = vertices;
    obj->normals  = normals;
    obj->vertexts = vertexts;

    obj->vert_indices    = vert_indices;
    obj->normal_indices  = normal_indices;
    obj->vertext_indices = vertext_indices;

    obj->face_num        = face_num;
}

static inline void PushRenderObject(Game *game, Object_Type object_type) {
    Renderer *renderer = game->renderer;
    switch (object_type) {
        case CHARACTER: {
            char *path = "data/meshes/character.obj";
            LoadObj(game, &renderer->objects[CHARACTER], path);
        } break;

        case FLOOR: {
            char *path = "data/meshes/floor.obj";
            LoadObj(game, &renderer->objects[FLOOR], path);
        } break;

        case CUBE: {
            char *path = "data/meshes/cube.obj";
            LoadObj(game, &renderer->objects[CUBE], path);
        } break;

        case TARGET_CIRCLE: {
            // Generate triangle vertices for a circle
            v4 p0 = {0, 0, 0, 1};
            v4 p1 = {-1, 0, 0, 1};
            f32 segment_len = 0.1f;
            s32 sections = (s32)(ceil(TAU/segment_len));

            v4 *vertices        = PushArray(game->memory, v4, sections * 3);
            v4 *vert_indices    = PushArray(game->memory, v4, sections);

            v4 *normals         = PushArray(game->memory, v4, sections * 3);
            v4 *normal_indices  = PushArray(game->memory, v4, sections);

            v4 *vertexts        = PushArray(game->memory, v4, sections * 3);
            v4 *vertext_indices = PushArray(game->memory, v4, sections);
            // Start from top left corner.
            f32 degree = 360.0f/sections;
            for (s32 i = 0; i < sections; ++i) {
                v4 p2 = {-(f32)cos(degree * TAU/360), (f32)sin(degree * TAU/360), 0.0f, 1.0f};

                vertices[i * 3]     = p1;
                vertices[i * 3 + 1] = p0;
                vertices[i * 3 + 2] = p2;

                vert_indices[i] = {f32(i*3+1), f32(i*3 + 2), f32(i*3 + 3), 0.0f};
                degree += 360.0f/sections;
                p1 = p2;

                // Init normals & vertexts
                normals[i * 3]     = {};
                normals[i * 3 + 1] = {};
                normals[i * 3 + 2] = {};
                normal_indices[i] = {f32(i*3+1), f32(i*3 + 2), f32(i*3 + 3), 0.0f};

                vertexts[i * 3]     = {};
                vertexts[i * 3 + 1] = {};
                vertexts[i * 3 + 2] = {};
                vertext_indices[i] = {f32(i*3+1), f32(i*3 + 2), f32(i*3 + 3), 0.0f};
            }
            
            InitObj(game, &renderer->objects[TARGET_CIRCLE], vertices, normals, vertexts, vert_indices, normal_indices, vertext_indices, sections * 3, sections);
        } break;
    }
}

// @CLEANUP: Initialize all objects first and put them in renderer instead of creating a new object
// for each mesh.
static inline void 
PushRenderMesh(Renderer *renderer, Tile_Map *tile_map, v3 tile_pos, m4x4 transform, Materials mat_type, Object_Type obj_type, Texture *texture, s32 color)
{
    Mesh *meshes = renderer->meshes;
    Obj obj = renderer->objects[obj_type]; 

    Tile *tile = GetTile(tile_map, tile_pos);
    tile->is_occupied = true;

    InitMesh(&meshes[renderer->mesh_count], &obj, tile->to_visual_position * transform, obj_type, mat_type, texture, color);

    renderer->mesh_count++;
}

// TODO: Fix see through pixels sometimes due to floating point math.
// This should use DrawTriangles in the inner loop.
static inline void 
DrawMeshes(Renderer *renderer, Camera *camera, f32 *zbuffer, bool shadow_pass) {
    Rect *rect    = renderer->screen;
    Mesh *meshes  = renderer->meshes;

    __m256 e0  = _mm256_set1_ps(renderer->shadow_transform.m[0]);
    __m256 e1  = _mm256_set1_ps(renderer->shadow_transform.m[1]);
    __m256 e2  = _mm256_set1_ps(renderer->shadow_transform.m[2]);
    __m256 e3  = _mm256_set1_ps(renderer->shadow_transform.m[3]);
    __m256 e4  = _mm256_set1_ps(renderer->shadow_transform.m[4]);
    __m256 e5  = _mm256_set1_ps(renderer->shadow_transform.m[5]);
    __m256 e6  = _mm256_set1_ps(renderer->shadow_transform.m[6]);
    __m256 e7  = _mm256_set1_ps(renderer->shadow_transform.m[7]);
    __m256 e8  = _mm256_set1_ps(renderer->shadow_transform.m[8]);
    __m256 e9  = _mm256_set1_ps(renderer->shadow_transform.m[9]);
    __m256 e10 = _mm256_set1_ps(renderer->shadow_transform.m[10]);
    __m256 e11 = _mm256_set1_ps(renderer->shadow_transform.m[11]);
    __m256 e12 = _mm256_set1_ps(renderer->shadow_transform.m[12]);
    __m256 e13 = _mm256_set1_ps(renderer->shadow_transform.m[13]);
    __m256 e14 = _mm256_set1_ps(renderer->shadow_transform.m[14]);
    __m256 e15 = _mm256_set1_ps(renderer->shadow_transform.m[15]);


    for (s32 i = 0; i < renderer->mesh_count; ++i) {
        Mesh *mesh = &meshes[i];
        mesh->bounding_box = {(f32)rect->width, (f32)rect->height, 0.0f, 0.0f};

        f32 blend_value = (f32)(u8)(mesh->color >> 24) / 256.0f;
        m4x4 final_transform = camera->projection * camera->lookat * mesh->transform;

        if (!mesh->enable_draw) continue;
        for (s32 j = 0; j < mesh->face_num; ++j) {
            v4 a = mesh->vertices[(s32)(mesh->vert_indices[j].e[0]) - 1];
            v4 b = mesh->vertices[(s32)(mesh->vert_indices[j].e[1]) - 1];
            v4 c = mesh->vertices[(s32)(mesh->vert_indices[j].e[2]) - 1];

            v4 an = mesh->normals[(s32)(mesh->normal_indices[j].e[0]) - 1];
            v4 bn = mesh->normals[(s32)(mesh->normal_indices[j].e[1]) - 1];
            v4 cn = mesh->normals[(s32)(mesh->normal_indices[j].e[2]) - 1];

            v4 at = mesh->vertexts[(s32)(mesh->vertext_indices[j].e[0]) - 1];
            v4 bt = mesh->vertexts[(s32)(mesh->vertext_indices[j].e[1]) - 1];
            v4 ct = mesh->vertexts[(s32)(mesh->vertext_indices[j].e[2]) - 1];

            a = final_transform * a;
            b = final_transform * b;
            c = final_transform * c;

            if (a.w <= 0.f || b.w <= 0.f || c.w <= 0.f) break;

            f32 aw = a.w;
            f32 bw = b.w;
            f32 cw = c.w;

            a = a/aw;
            b = b/bw;
            c = c/cw;

            a = camera->viewport * a;
            b = camera->viewport * b;
            c = camera->viewport * c;

            // Depth consts
            f32 az = 1.0f/aw;
            f32 bz = 1.0f/bw;
            f32 cz = 1.0f/cw;

            __m256 aZ_  = _mm256_set1_ps(az);
            __m256 abZ_ = _mm256_set1_ps(bz - az);
            __m256 acZ_ = _mm256_set1_ps(cz - az);

            __m256 az_  = _mm256_set1_ps(a.z);
            __m256 abz_ = _mm256_set1_ps(b.z - a.z);
            __m256 acz_ = _mm256_set1_ps(c.z - a.z);

            // Find the bounding box of the triangle.
            s32 min_x = (s32)GetMinFloor(a.x, b.x, c.x);
            s32 min_y = (s32)GetMinFloor(a.y, b.y, c.y);

            s32 max_x = (s32)GetMaxCeil(a.x, b.x, c.x);
            s32 max_y = (s32)GetMaxCeil(a.y, b.y, c.y);

            if (min_x < 0) min_x = 0;
            if (min_y < 0) min_y = 0;

            if (max_x >= rect->width) max_x = rect->width - 1;
            if (max_y >= rect->height) max_y = rect->height - 1;

            // Rect bounding box does not go past screen.
            if (min_x < mesh->bounding_box.x0) {
                mesh->bounding_box.x0 = (f32)min_x;
            }

            if (min_y < mesh->bounding_box.y0) {
                mesh->bounding_box.y0 = (f32)min_y;
            }

            if (max_x > mesh->bounding_box.x1) {
                mesh->bounding_box.x1 = (f32)max_x;
            }

            if (max_y > mesh->bounding_box.y1) {
                mesh->bounding_box.y1 = (f32)max_y;
            }

            // UVs
            at = at/aw;
            bt = bt/bw;
            ct = ct/cw;

            __m256 abx_t = _mm256_set1_ps(bt.x - at.x);
            __m256 acx_t = _mm256_set1_ps(ct.x - at.x);

            __m256 aby_t = _mm256_set1_ps(bt.y - at.y);
            __m256 acy_t = _mm256_set1_ps(ct.y - at.y);

            __m256 ax_t  = _mm256_set1_ps(at.x);
            __m256 bx_t  = _mm256_set1_ps(bt.x);
            __m256 cx_t  = _mm256_set1_ps(ct.x);

            __m256 ay_t  = _mm256_set1_ps(at.y);
            __m256 by_t  = _mm256_set1_ps(bt.y);
            __m256 cy_t  = _mm256_set1_ps(ct.y);

            // Normals
            __m256 abx_n = _mm256_set1_ps(bn.x - an.x);
            __m256 acx_n = _mm256_set1_ps(cn.x - an.x);
            __m256 ax_n  = _mm256_set1_ps(an.x);

            __m256 aby_n = _mm256_set1_ps(bn.y - an.y);
            __m256 acy_n = _mm256_set1_ps(cn.y - an.y);
            __m256 ay_n  = _mm256_set1_ps(an.y);

            __m256 abz_n = _mm256_set1_ps(bn.z - an.z);
            __m256 acz_n = _mm256_set1_ps(cn.z - an.z);
            __m256 az_n  = _mm256_set1_ps(an.z);

            v4 p_start = {(f32)min_x, (f32)min_y};

            __m256 ws  = _mm256_set1_ps(EdgeTest(a, b, c));
            f32 w0_row_start = EdgeTest(b, c, p_start);
            f32 w1_row_start = EdgeTest(c, a, p_start);
            f32 w2_row_start = EdgeTest(a, b, p_start);

            f32 A01 = a.y - b.y;
            f32 A12 = b.y - c.y;
            f32 A20 = c.y - a.y;

            f32 B01 = b.x - a.x;
            f32 B12 = c.x - b.x;
            f32 B20 = a.x - c.x;

            s32 *pixel_row = (s32 *)rect->data + min_y * rect->width;
            for (s32 y = min_y; y < max_y; ++y)
            {
                f32 w0_row = w0_row_start + B12 * (y - min_y);
                f32 w1_row = w1_row_start + B20 * (y - min_y);
                f32 w2_row = w2_row_start + B01 * (y - min_y);

                for (s32 x = min_x; x < max_x; x += 8)
                {
                    s32 steps = x - min_x;

                    __m256 onestep_A01 = _mm256_set_ps((A01 * (steps+7)), (A01 * (steps+6)), (A01 * (steps+5)), (A01 * (steps+4)), (A01 * (steps+3)), (A01 * (steps+2)), (A01 * (steps+1)), (A01 * steps));
                    __m256 onestep_A12 = _mm256_set_ps((A12 * (steps+7)), (A12 * (steps+6)), (A12 * (steps+5)), (A12 * (steps+4)), (A12 * (steps+3)), (A12 * (steps+2)), (A12 * (steps+1)), (A12 * steps));
                    __m256 onestep_A20 = _mm256_set_ps((A20 * (steps+7)), (A20 * (steps+6)), (A20 * (steps+5)), (A20 * (steps+4)), (A20 * (steps+3)), (A20 * (steps+2)), (A20 * (steps+1)), (A20 * steps));

                    __m256 w0s = _mm256_add_ps(_mm256_set1_ps(w0_row), onestep_A12);
                    __m256 w1s = _mm256_add_ps(_mm256_set1_ps(w1_row), onestep_A20);
                    __m256 w2s = _mm256_add_ps(_mm256_set1_ps(w2_row), onestep_A01);

                    __m256 packed_0 = _mm256_set1_ps(0);
                    __m256 w0_mask = _mm256_cmp_ps(w0s, packed_0, _CMP_GE_OS);
                    __m256 w1_mask = _mm256_cmp_ps(w1s, packed_0, _CMP_GE_OS);
                    __m256 w2_mask = _mm256_cmp_ps(w2s, packed_0, _CMP_GE_OS);

                    __m256i pixel_mask = _mm256_cvtps_epi32
                    (
                        _mm256_and_ps(w0_mask, _mm256_and_ps(w1_mask, w2_mask))
                    );

                    if (_mm256_testz_si256(pixel_mask, pixel_mask)) continue;

                    // Bary coords.
                    __m256 w0 = _mm256_div_ps(w0s, ws);
                    __m256 w1 = _mm256_div_ps(w1s, ws);
                    __m256 w2 = _mm256_div_ps(w2s, ws);

                    // P depths.
                    __m256 pz = _mm256_add_ps(az_, _mm256_add_ps(_mm256_mul_ps(abz_, w1), _mm256_mul_ps(acz_, w2)));
                    // Add depth offset for shadow buffer render coordinates to shift them away 
                    // from the light. This alleviate z-fighting comparing transformed pixel's depth 
                    // with the shadow buffer's depth at that location.
                    //
                    // This is not good enough yet as it introduces peter panning, and if the
                    // z_bias value is too low, it doesn't offset enough to make shadow acne go away.
                    if (shadow_pass) 
                    {
                        f32 z_bias = 0.00005f;
                        pz = _mm256_add_ps(pz, _mm256_set1_ps(-z_bias));
                    }

                    __m256i px = _mm256_set_epi32(x + 7, x + 6, x + 5, x + 4, x + 3, x + 2, x + 1, x);
                    __m256i py = _mm256_set1_epi32(y);

                    // P UVs
                    __m256 px_uv = _mm256_add_ps(_mm256_mul_ps(ax_t, w0), _mm256_mul_ps(bx_t, w1));
                    __m256 py_uv = _mm256_add_ps(_mm256_mul_ps(ay_t, w0), _mm256_mul_ps(by_t, w1));

                    // Perspective correct Z
                    px_uv = _mm256_add_ps(_mm256_mul_ps(cx_t, w2), px_uv);
                    py_uv = _mm256_add_ps(_mm256_mul_ps(cy_t, w2), py_uv);

                    __m256 pZ = _mm256_div_ps(_mm256_set1_ps(1.0f), _mm256_add_ps(aZ_, _mm256_add_ps(_mm256_mul_ps(abZ_, w1), _mm256_mul_ps(acZ_, w2))));

                    px_uv = _mm256_mul_ps(px_uv,pZ);
                    py_uv = _mm256_mul_ps(py_uv,pZ);
                    //px_uv = _mm256_div_ps(px_uv, _mm256_div_ps(_mm256_set1_ps(1), pz));
                    //py_uv = _mm256_div_ps(py_uv, _mm256_div_ps(_mm256_set1_ps(1), pz));

                    px_uv = _mm256_mul_ps(px_uv, _mm256_set1_ps((f32)mesh->texture.width));
                    py_uv = _mm256_mul_ps(py_uv, _mm256_set1_ps((f32)mesh->texture.height));

                    // P normals.
                    __m256 px_normal = _mm256_add_ps(_mm256_mul_ps(abx_n, w1), _mm256_mul_ps(acx_n, w2));
                    px_normal = _mm256_add_ps(ax_n, px_normal);

                    __m256 py_normal = _mm256_add_ps(_mm256_mul_ps(aby_n, w1), _mm256_mul_ps(acy_n, w2));
                    py_normal = _mm256_add_ps(ay_n, py_normal);

                    __m256 pz_normal = _mm256_add_ps(_mm256_mul_ps(abz_n, w1), _mm256_mul_ps(acz_n, w2));
                    pz_normal = _mm256_add_ps(az_n, pz_normal);

                    __m256 p_length = _mm256_add_ps(_mm256_mul_ps(px_normal, px_normal), _mm256_mul_ps(py_normal, py_normal));
                    p_length = _mm256_add_ps(p_length, _mm256_mul_ps(pz_normal, pz_normal));
                    p_length = _mm256_sqrt_ps(p_length);

                    px_normal = _mm256_div_ps(px_normal, p_length);
                    py_normal = _mm256_div_ps(py_normal, p_length);
                    pz_normal = _mm256_div_ps(pz_normal, p_length);

                    __m256 intensity = _mm256_add_ps
                    (
                        _mm256_add_ps
                        ( 
                            _mm256_mul_ps(px_normal, _mm256_set1_ps(camera->light_dir.x)), 
                            _mm256_mul_ps(py_normal, _mm256_set1_ps(camera->light_dir.y))
                        ), 
                        _mm256_mul_ps
                        (
                            pz_normal, 
                            _mm256_set1_ps(camera->light_dir.z)
                        )
                    );

                    s32 current_z_indices = rect->width * y + x;
                    __m256i z_indices = _mm256_set_epi32(current_z_indices+7,current_z_indices+6,current_z_indices+5,current_z_indices+4,current_z_indices+3,current_z_indices+2,current_z_indices+1,current_z_indices);

#if 1
                    __m256i pixel_clip_mask_gt = _mm256_cmpgt_epi32(px, _mm256_set1_epi32(rect->width - 1));

                    pixel_mask = _mm256_andnot_si256(pixel_clip_mask_gt, pixel_mask);


                    __m256 z_values = _mm256_i32gather_ps(zbuffer, z_indices, 4);
                    __m256i z_mask = _mm256_cvtps_epi32(_mm256_cmp_ps(pz, z_values, _CMP_GT_OS));

                    pixel_mask = _mm256_and_si256(pixel_mask, z_mask);
                    _mm256_maskstore_ps(zbuffer + y * rect->width + x, pixel_mask, pz);

                    if (shadow_pass) {
                        continue;
                    }

                    if (renderer->options != SHADOW_OFF) {
                        f32 half_width  = rect->width  * 0.5f;
                        f32 half_height = rect->height * 0.5f;

                        /*
                        for (s32 l = 0; l < 8; ++l)
                        {
                            if (pixel_mask.m256i_i32[l] == 0) continue;
                            f32 *shadow_zbuffer = renderer->shadow_zbuffer;
                            f32 ori_px = (f32)px.m256i_i32[l]/rect->width  * 2.0f - 1.0f;
                            f32 ori_py = (f32)py.m256i_i32[l]/rect->height * 2.0f - 1.0f;
                            f32 ori_pz = pz.m256_f32[l];

                            ori_px *= pZ.m256_f32[l];
                            ori_py *= pZ.m256_f32[l];
                            ori_pz *= pZ.m256_f32[l];

                            v4 p = {ori_px, ori_py, ori_pz, pZ.m256_f32[l]};

                            p = renderer->shadow_transform * p;
                            f32 ori_pw = p.w;
                            p = p/p.w;

                            s32 shadow_px = (s32)((p.x + 1) * 0.5f * rect->width);
                            s32 shadow_py = (s32)((p.y + 1) * 0.5f * rect->height);

                            s32 shadow_zindex = shadow_py * rect->width + shadow_px;
                            if (shadow_zindex >= 0 && shadow_zindex < rect->area)
                            {
                                if (shadow_zbuffer[shadow_zindex] > p.z)
                                {
                                    s32 *pixel = (s32 *)(pixel_row + px.m256i_i32[l]);
                                    *pixel = 0x00000000;
                                    pixel_mask.m256i_i32[l] = 0;
                                }
                            }
                        }
                        */

                        __m256 ori_px = _mm256_mul_ps(_mm256_add_ps(_mm256_mul_ps(_mm256_cvtepi32_ps(px), _mm256_set1_ps(2.0f/rect->width)),  _mm256_set1_ps(-1.0f)), pZ);
                        __m256 ori_py = _mm256_mul_ps(_mm256_add_ps(_mm256_mul_ps(_mm256_cvtepi32_ps(py), _mm256_set1_ps(2.0f/rect->height)), _mm256_set1_ps(-1.0f)), pZ);
                        __m256 ori_pz = _mm256_mul_ps(pz, pZ);
                        __m256 ori_pw = pZ;

                        __m256 new_px = _mm256_add_ps(_mm256_add_ps(_mm256_mul_ps(e0,  ori_px), _mm256_mul_ps(e1,  ori_py)), _mm256_add_ps(_mm256_mul_ps(e2,  ori_pz), _mm256_mul_ps(e3,  ori_pw)));
                        __m256 new_py = _mm256_add_ps(_mm256_add_ps(_mm256_mul_ps(e4,  ori_px), _mm256_mul_ps(e5,  ori_py)), _mm256_add_ps(_mm256_mul_ps(e6,  ori_pz), _mm256_mul_ps(e7,  ori_pw)));
                        __m256 new_pz = _mm256_add_ps(_mm256_add_ps(_mm256_mul_ps(e8,  ori_px), _mm256_mul_ps(e9,  ori_py)), _mm256_add_ps(_mm256_mul_ps(e10, ori_pz), _mm256_mul_ps(e11, ori_pw)));
                        __m256 new_pw = _mm256_add_ps(_mm256_add_ps(_mm256_mul_ps(e12, ori_px), _mm256_mul_ps(e13, ori_py)), _mm256_add_ps(_mm256_mul_ps(e14, ori_pz), _mm256_mul_ps(e15, ori_pw)));

                        new_px = _mm256_div_ps(new_px, new_pw);
                        new_py = _mm256_div_ps(new_py, new_pw);
                        new_pz = _mm256_div_ps(new_pz, new_pw);
                        new_pw = _mm256_div_ps(new_pw, new_pw);

                        __m256i shadow_px = _mm256_cvtps_epi32(_mm256_mul_ps(_mm256_add_ps(new_px, _mm256_set1_ps(1)), _mm256_set1_ps(rect->width * 0.5f)));
                        __m256i shadow_py = _mm256_cvtps_epi32(_mm256_mul_ps(_mm256_add_ps(new_py, _mm256_set1_ps(1)), _mm256_set1_ps(rect->height * 0.5f)));

                        __m256i shadow_zindices = _mm256_add_epi32(_mm256_mullo_epi32(shadow_py, _mm256_set1_epi32(rect->width)), shadow_px);

                        __m256i shadow_mask = _mm256_andnot_si256(_mm256_cmpgt_epi32(shadow_zindices, _mm256_set1_epi32(rect->area)), _mm256_cmpgt_epi32(shadow_zindices, _mm256_set1_epi32(-1)));

                        __m256 src = {};
                        __m256 shadow_vals = _mm256_mask_i32gather_ps(src, renderer->shadow_zbuffer, shadow_zindices, _mm256_cvtepi32_ps(shadow_mask), 4);

                        shadow_mask = _mm256_and_si256(shadow_mask, _mm256_cvtps_epi32(_mm256_cmp_ps(shadow_vals, new_pz, _CMP_GT_OS)));
                        s32 *pixels = (s32 *)(pixel_row + x);
                        shadow_mask = _mm256_and_si256(shadow_mask, z_mask);
                        _mm256_maskstore_epi32(pixels, shadow_mask, _mm256_set1_epi32(0));
                        pixel_mask = _mm256_andnot_si256(shadow_mask, pixel_mask);
                    }

                    s32 *pixel_vector = (s32 *)(pixel_row + x);

                    __m256 intensity_mask = _mm256_cmp_ps(intensity, packed_0, _CMP_GT_OS);
                    intensity = _mm256_blendv_ps(_mm256_set1_ps(0), intensity, intensity_mask);

                    if (mesh->texture.data) {
                        __m256i u = _mm256_cvtps_epi32(px_uv);
                        __m256i v = _mm256_cvtps_epi32(py_uv);

                        __m256i texture_clip_mask = _mm256_cmpgt_epi32(u, _mm256_set1_epi32(-1));
                        texture_clip_mask = _mm256_and_si256
                        (
                            _mm256_cmpgt_epi32(v, _mm256_set1_epi32(-1)),
                            texture_clip_mask
                        );
                        texture_clip_mask = _mm256_andnot_si256
                        (
                            _mm256_cmpgt_epi32(u, _mm256_set1_epi32(mesh->texture.width - 1)),
                            texture_clip_mask
                        );
                        texture_clip_mask = _mm256_andnot_si256
                        (
                            _mm256_cmpgt_epi32(v, _mm256_set1_epi32(mesh->texture.height - 1)),
                            texture_clip_mask
                        );

                        __m256i texture_indices = _mm256_add_epi32
                        (
                            u,
                            _mm256_mullo_epi32
                            (
                                _mm256_set1_epi32(mesh->texture.width),
                                v
                            )
                        );


                        s32 *p_texture = (s32 *)mesh->texture.data; 
                        __m256i def_vals = {};
                        __m256i texture_color = _mm256_mask_i32gather_epi32(def_vals, p_texture, texture_indices, texture_clip_mask, 4); 

                        __m256i red   = _mm256_srli_epi32(_mm256_slli_epi32(texture_color, 24), 8);
                        __m256i green = _mm256_slli_epi32(_mm256_srli_epi32(_mm256_slli_epi32(texture_color, 16), 24), 8);
                        __m256i blue  = _mm256_srli_epi32(_mm256_slli_epi32(texture_color, 8),  24);

                        __m256i pixel_color = _mm256_or_si256(red, _mm256_or_si256(green, blue));
                        _mm256_maskstore_epi32(pixel_vector, pixel_mask, pixel_color);
                    } else {
                        if (mesh->obj_type == TARGET_CIRCLE) { // Transparent for circles.
                            __m256 blend_value_256  = _mm256_set1_ps((f32)blend_value);

                            // Mesh color and channels, from windows color 0xAABBGGRR because windows wants 
                            // to see bitmap color order BGRA in memory.
                            __m256i color  = _mm256_set1_epi32(mesh->color);
                            __m256i red    = _mm256_srli_epi32(_mm256_slli_epi32(color, 24), 24);
                            __m256i green  = _mm256_srli_epi32(_mm256_slli_epi32(color, 16), 24);
                            __m256i blue   = _mm256_srli_epi32(_mm256_slli_epi32(color, 8),  24);

                            // Alpha-blend with the previous pixels of same locations.
                            __m256i prev_color = _mm256_maskload_epi32(pixel_vector, pixel_mask);

                            __m256i prev_red   = _mm256_srli_epi32(_mm256_slli_epi32(prev_color, 8),  24);
                            __m256i prev_green = _mm256_srli_epi32(_mm256_slli_epi32(prev_color, 16), 24);
                            __m256i prev_blue  = _mm256_srli_epi32(_mm256_slli_epi32(prev_color, 24), 24);

                            red   = _mm256_cvtps_epi32(_mm256_fmadd_ps(blend_value_256, _mm256_cvtepi32_ps(prev_red),   _mm256_fnmadd_ps(blend_value_256, _mm256_cvtepi32_ps(red),   _mm256_cvtepi32_ps(red))));
                            green = _mm256_cvtps_epi32(_mm256_fmadd_ps(blend_value_256, _mm256_cvtepi32_ps(prev_green), _mm256_fnmadd_ps(blend_value_256, _mm256_cvtepi32_ps(green), _mm256_cvtepi32_ps(green))));
                            blue  = _mm256_cvtps_epi32(_mm256_fmadd_ps(blend_value_256, _mm256_cvtepi32_ps(prev_blue),  _mm256_fnmadd_ps(blend_value_256, _mm256_cvtepi32_ps(blue),  _mm256_cvtepi32_ps(blue))));

                            // Color in order of 0xAARRGGBB to store later.
                            red   = _mm256_slli_epi32(red,   16);
                            green = _mm256_slli_epi32(green, 8);
                            blue  = _mm256_slli_epi32(blue,  0);

                            __m256i pixel_color = _mm256_or_si256(red, _mm256_or_si256(green, blue));

                            _mm256_maskstore_epi32(pixel_vector, pixel_mask, pixel_color);
                        } else {
                            __m256i blue  = _mm256_cvtps_epi32(_mm256_mul_ps(intensity, _mm256_set1_ps(255)));
                            __m256i red   = _mm256_slli_epi32(blue, 8);
                            __m256i green = _mm256_slli_epi32(blue, 16);

                            __m256i pixel_color = _mm256_or_si256(red, _mm256_or_si256(green, blue));
                            _mm256_maskstore_epi32(pixel_vector, pixel_mask, pixel_color);
                        }
                    }
#endif
                    // SCALAR PIXEL LOOP FOR REFERENCE.
#if 0
                    for (s32 l = 0; l < 8; ++l)
                    {
                        // Skip pixels out of range.
                        if (px.m256i_i32[l] >= rect->width)
                        {
                            continue;
                        }

                        if (w0s.m256_f32[l] >= 0.0f && 
                            w1s.m256_f32[l] >= 0.0f &&
                            w2s.m256_f32[l] >= 0.0f)
                        {
                            // If the calculated z index is out of the buffer memory range, just skip.

                            if (zbuffer[z_indices.m256i_i32[l]] < pz.m256_f32[l])
                            {
                                zbuffer[z_indices.m256i_i32[l]] = pz.m256_f32[l];
                                if (shadow_pass) continue; // Fill the shadow buffer then move on.

                                // Check pixel against shadow buffer.
                                if (renderer->options != SHADOW_OFF)
                                {
                                    f32 *shadow_zbuffer = renderer->shadow_zbuffer;
                                    f32 ori_px = (f32)px.m256i_i32[l]/rect->width * 2.0f - 1.0f;
                                    f32 ori_py = (f32)py.m256i_i32[l]/rect->height * 2.0f - 1.0f;
                                    f32 ori_pz = pz.m256_f32[l];

                                    ori_px *= pZ.m256_f32[l];
                                    ori_py *= pZ.m256_f32[l];
                                    ori_pz *= pZ.m256_f32[l];

                                    v4 p = {ori_px, ori_py, ori_pz, pZ.m256_f32[l]};

                                    p = renderer->shadow_transform * p;
                                    f32 ori_pw = p.w;
                                    p = p/p.w;

                                    s32 shadow_px = (s32)((p.x + 1) * 0.5f * rect->width);
                                    s32 shadow_py = (s32)((p.y + 1) * 0.5f * rect->height);

                                    s32 shadow_zindex = shadow_py * rect->width + shadow_px;
                                    if (shadow_zindex >= 0 && shadow_zindex < rect->area)
                                    {
                                        if (shadow_zbuffer[shadow_zindex] > p.z)
                                        {
                                            s32 *pixel = (s32 *)(pixel_row + px.m256i_i32[l]);
                                            *pixel = 0x00000000;
                                            continue;
                                        }
                                    }
                                }

                                if (intensity.m256_f32[l] > 0)
                                {
                                    if (mesh->texture.data)
                                    {
                                        if ((s32)py_uv.m256_f32[l] < mesh->texture.height &&
                                                (s32)px_uv.m256_f32[l] < mesh->texture.width &&
                                                (s32)py_uv.m256_f32[l] >= 0 &&
                                                (s32)px_uv.m256_f32[l] >= 0)
                                        {
                                            s32 *p_texture = (s32 *)mesh->texture.data + mesh->texture.width * (s32)py_uv.m256_f32[l] + (s32)px_uv.m256_f32[l];

                                            u8 *pixel = (u8 *)(pixel_row + px.m256i_i32[l]);
                                            v4 pixel_color = {};
                                            pixel_color.x = *((u8 *)p_texture);
                                            pixel_color.y = *((u8 *)p_texture + 1);
                                            pixel_color.z = *((u8 *)p_texture + 2);

                                            f32 diffuse_intensity = intensity.m256_f32[l];
                                            f32 ambient_intensity = 0.2f;
                                            f32 total_intensity   = diffuse_intensity + ambient_intensity;

                                            pixel_color.x = pixel_color.x * total_intensity;
                                            pixel_color.y = pixel_color.y * total_intensity;
                                            pixel_color.z = pixel_color.z * total_intensity;

                                            if (pixel_color.x > 255) pixel_color.x = 255;
                                            if (pixel_color.y > 255) pixel_color.y = 255;
                                            if (pixel_color.z > 255) pixel_color.z = 255;

                                            // @SPEED: Why is this so much slower than above in debug.
                                            //pixel_color = pixel_color * total_intensity;

                                            *(s32 *)pixel = (s32)pixel_color.x << 16 |
                                                            (s32)pixel_color.y << 8  |
                                                            (s32)pixel_color.z;
                                        }
                                    }
                                    else
                                    {
                                        u8 *pixel = (u8 *)(pixel_row + px.m256i_i32[l]);
                                        *pixel = (u8)((u8)(mesh->color >> 0) * intensity.m256_f32[l]);
                                        pixel++;
                                        *pixel = (u8)((u8)(mesh->color >> 8) * intensity.m256_f32[l]);
                                        pixel++;
                                        *pixel = (u8)((u8)(mesh->color >> 16) * intensity.m256_f32[l]);
                                    }
                                }
                                else
                                {
                                    u8 *pixel = (u8 *)(pixel_row + px.m256i_i32[l]);
                                    *pixel = (u8)((u8)(mesh->color >> 0) * 0.2f);
                                    pixel++;
                                    *pixel = (u8)((u8)(mesh->color >> 8) * 0.2f);
                                    pixel++;
                                    *pixel = (u8)((u8)(mesh->color >> 16) * 0.2f);
                                }
                            }
                        }
                    }
#endif
                }

                pixel_row += rect->width;
            }
        }
    }
}

// Old version of DrawMeshes for testing algorithm before being SIMD'd. This is not updated frequently.
/*
static inline
void DrawMeshesSlow(Mesh *meshes, Rect *rect, Camera *camera, f32 *zbuffer, i32 mesh_count)
{
    for (i32 i = 0; i < mesh_count; ++i)
    {
        Mesh *mesh = &meshes[i];

        if (!mesh->enable_draw) continue;
        for (i32 j = 0; j < mesh->face_num; ++j)
        {
            v4 a = mesh->vertices[(i32)(mesh->vert_indices[j].e[0]) - 1];
            v4 b = mesh->vertices[(i32)(mesh->vert_indices[j].e[1]) - 1];
            v4 c = mesh->vertices[(i32)(mesh->vert_indices[j].e[2]) - 1];

            v4 an = mesh->normals[(i32)(mesh->normal_indices[j].e[0]) - 1];
            v4 bn = mesh->normals[(i32)(mesh->normal_indices[j].e[1]) - 1];
            v4 cn = mesh->normals[(i32)(mesh->normal_indices[j].e[2]) - 1];

            v4 at = mesh->vertexts[(i32)(mesh->vertext_indices[j].e[0]) - 1];
            v4 bt = mesh->vertexts[(i32)(mesh->vertext_indices[j].e[1]) - 1];
            v4 ct = mesh->vertexts[(i32)(mesh->vertext_indices[j].e[2]) - 1];

            a = camera->projection * camera->lookat * camera->translate * camera->rotate * mesh->transform * a;
            b = camera->projection * camera->lookat * camera->translate * camera->rotate * mesh->transform * b;
            c = camera->projection * camera->lookat * camera->translate * camera->rotate * mesh->transform * c;

            if (a.w <= 0.f || b.w <= 0.f || c.w <= 0.f) break;

            f32 aw = a.w;
            f32 bw = b.w;
            f32 cw = c.w;

            // With reverse z mapping, z is in range [1, 0] after w divide.
            a = a/aw;
            b = b/bw;
            c = c/cw;

            a = camera->viewport * a;
            b = camera->viewport * b;
            c = camera->viewport * c;

            // Find the bounding box of the triangle.
            i32 min_x = (i32)GetMinFloor(a.x, b.x, c.x);
            i32 min_y = (i32)GetMinFloor(a.y, b.y, c.y);

            i32 max_x = (i32)GetMaxCeil(a.x, b.x, c.x);
            i32 max_y = (i32)GetMaxCeil(a.y, b.y, c.y);

            if (min_x < 0) min_x = 0;
            if (min_y < 0) min_y = 0;

            if (max_x >= rect->width) max_x = rect->width - 1;
            if (max_y >= rect->height) max_y = rect->height - 1;

            // UVs && normals
            at = at/aw;
            bt = bt/bw;
            ct = ct/cw;

            an = an/aw;
            bn = bn/bw;
            cn = cn/cw;

            i32 *pixel_row = (i32 *)rect->data + min_y * rect->width;
            for (i32 y = min_y; y <= max_y; ++y)
            {
                for (i32 x = min_x; x <= max_x; ++x)
                {
                    // Bary coords.
                    v4 p = {(f32)x, (f32)y, 0};
                    f32 edge_ab = EdgeTest(a, b, p);
                    f32 edge_bc = EdgeTest(b, c, p);
                    f32 edge_ca = EdgeTest(c, a, p);
                    if (edge_ab >= 0 && edge_bc >= 0 && edge_ca >= 0)
                    {
                        v4 bary = GetBaryCoords(a, b, c, {(f32)x, (f32)y, 0, 1});

                        v4 pn = Normalize(an * bary.x + bn * bary.y + cn * bary.z);
                        p.z = bary.x*a.z + bary.y*b.z + bary.z*c.z;
                        i32 z_index = x + y * rect->width;
                        f32 intensity = Dot(pn, camera->light_dir);

                        static i32 counter = 0;
                        counter++;
                        if (zbuffer[z_index] < p.z && z_index < rect->area && z_index >= 0)
                        {
                            zbuffer[z_index] = p.z;

                            if (intensity >= 0)
                            {
                                if (mesh->texture.data)
                                {
                                    f32 pz = bary.x/aw + bary.y/bw + bary.z/cw;
                                    v4 pt = at * bary.x + bt * bary.y + ct * bary.z;
                                    pt = pt/pz;
                                    pt.x = mesh->texture.width * pt.x;
                                    pt.y = mesh->texture.height * pt.y;
                                    i32 *p_texture = (i32 *)mesh->texture.data + mesh->texture.width * (i32)pt.y + (i32)pt.x;

                                    u8 *pixel = (u8 *)(pixel_row + x);
                                    *pixel = (u8)(*((u8 *) p_texture  + 2) * intensity);
                                    pixel++;
                                    *pixel = (u8)(*((u8 *) p_texture  + 1) * intensity);
                                    pixel++;
                                    *pixel = (u8)(*((u8 *) p_texture  + 0) * intensity);
                                }
                                else
                                {
                                    u8 *pixel = (u8 *)(pixel_row + x);
                                    *pixel = (u8)(255 * intensity);
                                    pixel++;
                                    *pixel = (u8)(255 * intensity);
                                    pixel++;
                                    *pixel = (u8)(255 * intensity);
                                }
                            }
                            else
                            {
                                u8 *pixel = (u8 *)(pixel_row + x);
                                *pixel = (u8)(122);
                                pixel++;
                                *pixel = (u8)(122);
                                pixel++;
                                *pixel = (u8)(122);
                            }
                        }
                    }
                }

                pixel_row += rect->width;
            }
        }
    }
}
*/

// Byte order for color 0xAARRGGBB -> 0xBBGGRRAA in memory.
static inline 
void ClearScreen(Rect *screen, s32 color)
{
    __m256i colors = _mm256_set1_epi32(color);
    s32 *data = (s32 *)screen->data;
    for (s32 i = 0; i < screen->area; i += 4)
    {
        _mm256_store_si256((__m256i *)data, colors);
        data += 4;
    }
}

// Render font from bitmap.
static inline 
void DrawCharQuad(Rect *screen, Rect *bitmap, stbtt_aligned_quad q, s32 advance_y, s32 baseline)
{
    f32 out_width = q.x1 - q.x0;
    f32 out_height = q.y1 - q.y0;

    // Bitmap texture coordinates
    f32 quad_width = (q.s1 - q.s0) * bitmap->width;
    f32 quad_height = (q.t1 - q.t0) * bitmap->height;

    f32 min_x = q.s0 * bitmap->width;
    f32 min_y = q.t0 * bitmap->height;

    s32 offset = (s32)(q.y1 - baseline);
    offset = offset - baseline; // Final offset, including baseline.

    for (s32 y = (s32)q.y0; y < (s32)q.y1; ++y)
    {
        for (s32 x = (s32)q.x0; x < (s32)q.x1; ++x)
        {
            s32 bitmap_x = (s32)(min_x + 1 + ((f32)(x - q.x0)/out_width) * quad_width);
            s32 bitmap_y = (s32)(min_y + 1 + ((f32)(y - q.y0)/out_height) * quad_height);

            u8 data = *(bitmap->data + bitmap_y * bitmap->width + bitmap_x);

            if (data)
            {
                s32 screen_x = x;
                s32 screen_y = (s32)q.y1 - y - offset + advance_y;
                if (screen_y < 0 || screen_y >= screen->height) continue;
                if (screen_x < 0 || screen_x >= screen->width) continue;

                s32* screen_pixel = (s32 *)screen->data + screen_y * screen->width + screen_x;

                u8 *blue  = (u8 *)screen_pixel;
                u8 *green = (u8 *)screen_pixel + 1;
                u8 *red   = (u8 *)screen_pixel + 2;
                u8 *alpha = (u8 *)screen_pixel + 3;

                f32 blend = 1.0f - data/255.0f;

                *blue  = (u8)(data + *blue  * blend);
                *green = (u8)(data + *green * blend);
                *red   = (u8)(data + *red   * blend);
                *alpha = (u8)(data + *alpha * blend);
            }
        }
    }
}

// This is NOT a general bitmap draw operation. This is only a temporary/placeholder function to explore stb_truetype api paths. Not to be used in actual code.
static inline 
void DrawBitmap(Rect *screen, Rect *bitmap, s32 x_offset, s32 y_offset, s32 width, s32 height)
{
    for (s32 y = 0; y < height; ++y)
    {
        for (s32 x = 0; x < width; ++x)
        {
            s32 bitmap_x = (s32)(((f32)x/width)  * bitmap->width);
            s32 bitmap_y = (s32)(((f32)y/height) * bitmap->height);

            u8* data = bitmap->data + bitmap_y * bitmap->width + bitmap_x;
            if (*data)
            {
                s32 screen_x = x + x_offset;
                s32 screen_y = y + y_offset;
                if (screen_y < 0 || screen_y >= screen->height) continue;
                s32* screen_pixel = (s32 *)screen->data + screen_y * screen->width + screen_x;

                *screen_pixel = *data;
            }
        }
    }
}

static inline
void DrawTriangle(Rect *screen, Shape_Rect *r, v4 a, v4 b, v4 c, s32 color) {
    // Find the bounding box of the triangle.
    s32 x0 = (s32)r->x0;
    s32 y0 = (s32)r->y0;

    s32 x1 = (s32)r->x1;
    s32 y1 = (s32)r->y1;

    v4 p_start = {(f32)x0, (f32)y0};

    if (x0 < 0) x0 = 0;
    if (y0 < 0) y0 = 0;

    if (x1 >= screen->width) x1 = screen->width - 1;
    if (y1 >= screen->height) y1 = screen->height - 1;

    __m256 ws  = _mm256_set1_ps(EdgeTest(a, b, c));
    f32 w0_row_start = EdgeTest(b, c, p_start);
    f32 w1_row_start = EdgeTest(c, a, p_start);
    f32 w2_row_start = EdgeTest(a, b, p_start);

    f32 A01 = a.y - b.y;
    f32 A12 = b.y - c.y;
    f32 A20 = c.y - a.y;

    f32 B01 = b.x - a.x;
    f32 B12 = c.x - b.x;
    f32 B20 = a.x - c.x;
 
    s32 *pixel_row = (s32 *)screen->data + y0 * screen->width;
    for (s32 y = y0; y < y1; ++y) {
        f32 w0_row = w0_row_start + B12 * (y - y0);
        f32 w1_row = w1_row_start + B20 * (y - y0);
        f32 w2_row = w2_row_start + B01 * (y - y0);

        for (s32 x = x0; x < x1; x += 8) {
            s32 steps = x - x0;

            __m256 onestep_A01 = _mm256_set_ps((A01 * (steps+7)), (A01 * (steps+6)), (A01 * (steps+5)), (A01 * (steps+4)), (A01 * (steps+3)), (A01 * (steps+2)), (A01 * (steps+1)), (A01 * steps));
            __m256 onestep_A12 = _mm256_set_ps((A12 * (steps+7)), (A12 * (steps+6)), (A12 * (steps+5)), (A12 * (steps+4)), (A12 * (steps+3)), (A12 * (steps+2)), (A12 * (steps+1)), (A12 * steps));
            __m256 onestep_A20 = _mm256_set_ps((A20 * (steps+7)), (A20 * (steps+6)), (A20 * (steps+5)), (A20 * (steps+4)), (A20 * (steps+3)), (A20 * (steps+2)), (A20 * (steps+1)), (A20 * steps));

            __m256 w0s = _mm256_add_ps(_mm256_set1_ps(w0_row), onestep_A12);
            __m256 w1s = _mm256_add_ps(_mm256_set1_ps(w1_row), onestep_A20);
            __m256 w2s = _mm256_add_ps(_mm256_set1_ps(w2_row), onestep_A01);

            // Bary coords.
            __m256 w0 = _mm256_div_ps(w0s, ws);
            __m256 w1 = _mm256_div_ps(w1s, ws);
            __m256 w2 = _mm256_div_ps(w2s, ws);

            __m256i px = _mm256_set_epi32(x + 7, x + 6, x + 5, x + 4, x + 3, x + 2, x + 1, x);
            __m256i py = _mm256_set1_epi32(y);

            for (s32 l = 0; l < 8; ++l) {
                if (w0s.m256_f32[l] >= 0.0f && 
                    w1s.m256_f32[l] >= 0.0f &&
                    w2s.m256_f32[l] >= 0.0f) {
                    s32 current_x = px.m256i_i32[l];
                    s32 current_y = py.m256i_i32[l];

                    if (current_x >= 0 && current_y >= 0 && 
                        current_x < screen->width && current_y < screen->height) {
                        s32 *pixel = (s32 *)(pixel_row + px.m256i_i32[l]);

                        f32 blend_value = (f32)((u8)(color >> 24) / 255.0f);

                        u8 red   = (u8)Lerp((f32)((u8)(color)),       (f32)(u8)(*pixel),       blend_value);
                        u8 green = (u8)Lerp((f32)((u8)(color >> 8)),  (f32)(u8)(*pixel >> 8),  blend_value);
                        u8 blue  = (u8)Lerp((f32)((u8)(color >> 16)), (f32)(u8)(*pixel >> 16), blend_value);

                        *pixel = red | green << 8 | blue << 16;
                    }
                }
            }
        }

        pixel_row += screen->width;
    }
}

// Reverse the height since we draw bottom up.
static inline void DrawQuad(Rect *screen, Shape_Rect *r, s32 color) {
    v4 a = {r->x0, r->y1};
    v4 b = {r->x0, r->y0};
    v4 c = {r->x1, r->y0};
    v4 d = {r->x1, r->y1};

    DrawTriangle(screen, r, a, b, c, color);
    DrawTriangle(screen, r, c, d, a, color);
}

static inline void DrawCircle(Rect *screen, f32 offset_x, f32 offset_y, s32 radius, s32 color) {
    s32 sh = screen->height - 1; // Because height range [0, height - 1].

    // Counter clockwise from top left.
    Shape_Rect o0;
    Shape_Rect o1;
    Shape_Rect o2;
    Shape_Rect o3;

    s32 diameter = radius * 2;
    
    o0.x0 = offset_x - radius;
    o0.y0 = sh - offset_y;
    o0.x1 = offset_x;
    o0.y1 = sh - offset_y + radius;

    o1.x0 = offset_x - radius;
    o1.y0 = sh - offset_y - radius;
    o1.x1 = offset_x;
    o1.y1 = sh - offset_y;

    o2.x0 = offset_x;
    o2.y0 = sh - offset_y - radius;
    o2.x1 = offset_x + radius;
    o2.y1 = sh - offset_y;

    o3.x0 = offset_x;
    o3.y0 = sh - offset_y;
    o3.x1 = offset_x + radius;
    o3.y1 = sh - offset_y + radius;

    s32 subarc_pixels   = 5;
    s32 arc_length      = (s32)(TAU*radius * 0.25f);
    s32 sections        = arc_length / subarc_pixels;
    f32 dtheta          = 90.0f / sections;
    f32 theta           = dtheta;

    v4 l2 = {};
    v4 l1 = {};

    s32 x_dir = 0;
    s32 y_dir = 0;

    f32 radian = TAU/360.0f;
    
    for (s32 c = 0; c < 4; ++c) {
        if (c == 0) {
            l2 = {o0.x1, o0.y0};
            l1 = {o0.x0, o0.y0};

            x_dir = -1;
            y_dir =  1;
        }

        if (c == 1) {
            l2 = {o1.x1, o1.y1};
            l1 = {o1.x0, o1.y1};

            x_dir = -1;
            y_dir = -1;
        }

        if (c == 2) {
            l2 = {o2.x0, o2.y1};
            l1 = {o2.x1, o2.y1};

            x_dir =  1;
            y_dir = -1;
        }

        if (c == 3) {
            l2 = {o3.x0, o3.y0};
            l1 = {o3.x1, o3.y0};

            x_dir = 1;
            y_dir = 1;
        }

        for (s32 s = 0; s < sections; ++s) {
            f32 x = (f32)cos(theta*radian) * radius * x_dir;
            f32 y = (f32)sin(theta*radian) * radius * y_dir;

            v4 l0 = l1;
            l1 = {l2.x + x, l2.y + y};

            Shape_Rect r;
            r.x0 = (f32)GetMin((s32)floor(l0.x), (s32)floor(l1.x), (s32)floor(l2.x));
            r.y0 = (f32)GetMin((s32)floor(l0.y), (s32)floor(l1.y), (s32)floor(l2.y));

            r.x1 = (f32)GetMax((s32)ceil(l0.x), (s32)ceil(l1.x), (s32)ceil(l2.x));
            r.y1 = (f32)GetMax((s32)ceil(l0.y), (s32)ceil(l1.y), (s32)ceil(l2.y));


            if (c == 0) DrawTriangle(screen, &r, l1, l0, l2, color);
            if (c == 1) DrawTriangle(screen, &r, l2, l0, l1, color);
            if (c == 2) DrawTriangle(screen, &r, l1, l0, l2, color);
            if (c == 3) DrawTriangle(screen, &r, l0, l1, l2, color);

            theta += dtheta;

        }

        theta = dtheta;
    }
}

/*
static inline void DrawCircleMesh(Rect *screen, f32 offset_x, f32 offset_y, s32 radius, s32 color) {
    s32 sh = screen->height - 1; // Because height range [0, height - 1].

    // Counter clockwise from top left.
    Shape_Rect o0;
    Shape_Rect o1;
    Shape_Rect o2;
    Shape_Rect o3;

    s32 diameter = radius * 2;
    
    o0.x0 = offset_x - radius;
    o0.y0 = sh - offset_y;
    o0.x1 = offset_x;
    o0.y1 = sh - offset_y + radius;

    o1.x0 = offset_x - radius;
    o1.y0 = sh - offset_y - radius;
    o1.x1 = offset_x;
    o1.y1 = sh - offset_y;

    o2.x0 = offset_x;
    o2.y0 = sh - offset_y - radius;
    o2.x1 = offset_x + radius;
    o2.y1 = sh - offset_y;

    o3.x0 = offset_x;
    o3.y0 = sh - offset_y;
    o3.x1 = offset_x + radius;
    o3.y1 = sh - offset_y + radius;

    s32 subarc_pixels   = 5;
    s32 arc_length      = (s32)(TAU*radius * 0.25f);
    s32 sections        = arc_length / subarc_pixels;
    f32 dtheta          = 90.0f / sections;
    f32 theta           = dtheta;

    v4 l2 = {};
    v4 l1 = {};

    s32 x_dir = 0;
    s32 y_dir = 0;

    f32 radian = TAU/360.0f;
    
    for (s32 c = 0; c < 4; ++c) {
        if (c == 0) {
            l2 = {o0.x1, o0.y0};
            l1 = {o0.x0, o0.y0};

            x_dir = -1;
            y_dir =  1;
        }

        if (c == 1) {
            l2 = {o1.x1, o1.y1};
            l1 = {o1.x0, o1.y1};

            x_dir = -1;
            y_dir = -1;
        }

        if (c == 2) {
            l2 = {o2.x0, o2.y1};
            l1 = {o2.x1, o2.y1};

            x_dir =  1;
            y_dir = -1;
        }

        if (c == 3) {
            l2 = {o3.x0, o3.y0};
            l1 = {o3.x1, o3.y0};

            x_dir = 1;
            y_dir = 1;
        }

        for (s32 s = 0; s < sections; ++s) {
            f32 x = (f32)cos(theta*radian) * radius * x_dir;
            f32 y = (f32)sin(theta*radian) * radius * y_dir;

            v4 l0 = l1;
            l1 = {l2.x + x, l2.y + y};

            Shape_Rect r;
            r.x0 = (f32)GetMin((s32)floor(l0.x), (s32)floor(l1.x), (s32)floor(l2.x));
            r.y0 = (f32)GetMin((s32)floor(l0.y), (s32)floor(l1.y), (s32)floor(l2.y));

            r.x1 = (f32)GetMax((s32)ceil(l0.x), (s32)ceil(l1.x), (s32)ceil(l2.x));
            r.y1 = (f32)GetMax((s32)ceil(l0.y), (s32)ceil(l1.y), (s32)ceil(l2.y));


            if (c == 0) DrawTriangle(screen, &r, l1, l0, l2, color);
            if (c == 1) DrawTriangle(screen, &r, l2, l0, l1, color);
            if (c == 2) DrawTriangle(screen, &r, l1, l0, l2, color);
            if (c == 3) DrawTriangle(screen, &r, l0, l1, l2, color);

            theta += dtheta;

        }

        theta = dtheta;
    }
}
*/
