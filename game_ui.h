struct Font
{
    Rect bitmap;
    s32 first_char;
    s32 num_chars;
    f32 line_height;
    stbtt_packedchar *chardata;

    f32 ascent;
    f32 descent;
    f32 line_gap;

    s32 lower_case_visible_height;
    s32 upper_case_visible_height;
};

struct Button_Theme
{
    s32 color = 0x000000FF;
};

struct Button
{
    s32 id = 0x0;
    s32 frame_count;
    Button_Theme theme;
};

struct Game_UI {
    Button *buttons;
    Rect *screen;
    Font *font;
    Input *input;

    struct Selection *selection;

    s32 button_count = 0;
    s32 current_button_index = 0;
};
