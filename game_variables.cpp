bool IsValidVarName(char a)
{
    bool result = false;    

    if ( (a >= '0' && a <= '9') || 
         (a >= 'A' && a <= 'Z') || 
         (a >= 'a' && a <= 'z') ||
         (a == '_') ) result = true;

    return result;
}

s32 CountVar(char *str, s32 *locations, s32 str_len)
{
    s32 index = 0;
    s32 count = 0;
    while (index < str_len)
    {
        // TODO: This wants to be the full form @vars instead of just '@'
        if (str[index] == '@')
        {
            locations[count] = index;
            count++;
        }
            
        index++;
    }

    return count;
}

// NOTE: This is a guarantee, if not, it's a bug.
bool FindHead(char *str, s32* str_pos)
{
    bool found = false;
    bool brace_found = false;
    
    s32 pos = *str_pos;
    while (pos--)
    {
        if (str[pos] == '{')
        {
            brace_found = true;
            break;
        }
    }

    if (brace_found)
    {
        found = true;
        // Skip non-char backward to find struct name.
        while (!IsValidVarName(str[pos]))
        {
            pos--;
        }

        // Go back to the start of the struct name - 1 index.
        while (IsValidVarName(str[pos]))
        {
            pos--;
        }

        // Go to the start of struct name.
        pos++;
    }

    *str_pos = pos;
    return found;
}

void SkipNonVar(char *str, s32 *str_pos)
{
    s32 pos = *str_pos;
    while (!IsValidVarName(str[pos]))
    {
        pos++;
    }

    *str_pos = pos;
}

// Read through the variable file and get the locations of all the vars.
Vars_Info InitVarsList(Game *game)
{
    Platform *platform = game->platform_api;
    Game_Memory *memory = game->memory;

    File file = {};
    file.name = "game.variables";
    platform->OpenFile(&file);
    Scratch_Memory *scratch_mem = BeginGameScratch(memory);
    file.data = PushArrayScratch(scratch_mem, char, file.size);
    platform->ReadEntireFile(&file);
    platform->CloseFile(&file);
    
    s32 count = 0;
    for (s32 i = 0; i < file.size; ++i)
    {
        if (file.data[i] == '@') count++;
    }

    Vars_Info vars_list = {};

    vars_list.data = PushArray(memory, s32, count);

    vars_list.count = count;
    count = 0;
    for (s32 index = 0; index < file.size; ++index)
    {
        if (file.data[index] == '@')
        {
            // TODO: This crashes when temporary memory clears to 0. Figure it out.
            vars_list.data[count] = index;
            count++;
        }
    }

    EndScratch(scratch_mem);

    return vars_list;
}

// Find the var in the list then read into target_struct the values for the correct vars.
void ReadVars(Game *game, File* file, Vars_Info *vars_list, void *target_struct, char *struct_name)
{
    Platform *platform = game->platform_api;
    Game_Memory *memory = game->memory;

    if (vars_list->is_reloaded)
    {
        platform->OpenFile(file);
        Scratch_Memory *scratch_mem = BeginGameScratch(memory);
        file->data = PushArrayScratch(scratch_mem, char, file->size);
        platform->ReadEntireFile(file);
        platform->CloseFile(file);

        for (s32 var_index = 0; var_index < vars_list->count; ++var_index)
        {
            s32 index = vars_list->data[var_index] + 1;
            char current_struct_name[256] = {};
            s32 struct_name_index = 0;
            while (file->data[index] != '\n')
            {
                current_struct_name[struct_name_index] = file->data[index];
                struct_name_index++;
                index++;
            };

            if (strcmp(current_struct_name, struct_name) != 0)
            {
                continue;
            }
            index++;

            s32 line_count = 0;

            scan_line:
            if (index < file->size)
            {
                char name[256] = {0};
                char value[256] = {0};
                s32 i = 0;
                // Name.
                while (IsValidVarName(file->data[index]))
                {
                    name[i] = file->data[index];
                    index++;
                    i++;
                }

                while (file->data[index] == ' ')
                {
                    index++;
                }

                // Value.
                i = 0;
                while (IsValidVarName(file->data[index]) || file->data[index] == '-' || file->data[index] == '.')
                {
                    value[i] = file->data[index];
                    i++;
                    index++;
                }

                f32 *fvalue = (f32 *)target_struct + line_count;
                *fvalue = (f32)atof(value);

                line_count++;
                SkipNonVar(file->data, &index);
                goto scan_line;
            }
        }

        vars_list->is_reloaded = false;
        EndScratch(scratch_mem);
    }
    else
    {
        // Check for file change event from platform.
        platform->OpenFile(file);
        platform->GetFileTime(file);
        if (file->last_write_low != file->prev_write_low || file->last_write_high != file->prev_write_high)
        {
            vars_list->is_reloaded = true;
            file->prev_write_low  = file->last_write_low;
            file->prev_write_high = file->last_write_high;
        }
        platform->CloseFile(file);
    }
}

bool CheckVarsReload(Platform *platform, File *file, Vars_Info *vars_list)
{
    // Check for file change event from platform.
    platform->OpenFile(file);
    platform->GetFileTime(file);
    if (file->last_write_low != file->prev_write_low || file->last_write_high != file->prev_write_high)
    {
        vars_list->is_reloaded = true;
        file->prev_write_low  = file->last_write_low;
        file->prev_write_high = file->last_write_high;
    }
    platform->CloseFile(file);

    return vars_list->is_reloaded;
}
